<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpeninChatbot extends Model
{
    use HasFactory;
    public function getOpeniChatbot()
    {
        {
            
                 $rs = [
                    '0' => ['id' => 'y',  'name' => 'Yes', 'selected' => true],
                    '1' => ['id' => 'n',  'name' => 'No', 'selected' => false]
                ];
    
            return $rs;
        }
    }
}
