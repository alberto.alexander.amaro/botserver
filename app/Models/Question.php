<?php

/**
 * Application: Spertatek
 * File       : Question.php
 * Type       : Model
 * Function   : Data manipulation for table question
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Question extends Model
{
    use HasFactory;
    protected $table = 'question';
    protected $primaryKey = 'questionId';
    protected $fillable = [
        'questionName',
        'scope',
        'orgId'
    ];

    public function getQuestionOrderById()
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true) {
            return $this
                ->join('organization', 'organization.orgId', 'question.orgId')
                ->join('scope', 'scope.scopeId', 'question.scope')
                ->orderBy('questionId', 'ASC')
                ->get();
        } else {
            return $this->join('organization', 'organization.orgId', 'question.orgId')
                ->join('scope', 'scope.scopeId', 'question.scope')
                ->where('question.orgId', Auth::user()->organization->orgId)
                ->orderBy('questionId', 'ASC')
                ->get();
        }
    }


    public function getQuestionSelectById()
    {
        return $this->pluck('categoryName', 'categoryId');
    }

    public function addQuestion($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $question                = new Question;

            $question->questionName  = $data['questionName'];
            $question->scope  = $data['scope'];
            $question->orgId  = $data['orgId'];
            $question->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function findById($id)
    {
        $rs = $this->findOrfail($id);
        return $rs;
    }

    public function updateQuestion($id, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $question  = Question::find($id);
            $question->questionName  = $data['questionName'];
            $question->scope  = $data['scope'];
            $question->orgId  = $data['orgId'];

            $question->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated') , 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function deleteQuestion($id)
    {
        $rs = $this->findOrfail($id);
        if ($rs->word_question->count() != 0) {
            return $rs = ['class' => 'error', 'alert' =>  trans('multi-lang.enodeleted'), 'message' => trans('multi-lang.nodeleted')];
        } else {
            try {
                $rs->delete();
                $success = true;
            } catch (\Exception $e) {
                $error   = $e->getMessage();
                $success = false;
            }

            if ($success) {
                return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
            } else {
                return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            }
        }
    }

    public function getQuestionSelectSolutionLinkByName($id, $orgId, $scope)
    {

        $rs =  $this->leftJoin('question_solution', 'question.questionId', 'question_solution.questionId')
            ->where('solutionId', '=', $id)
            ->get(['questionName', 'question.questionId', 'question_solution.solutionId'])
            ->pluck('questionName', 'questionId');

        $host = $this
            ->where('orgId', '=', $orgId)
            ->where('scope', '=', $scope)
            ->pluck('questionName', 'questionId');

        $collection = collect($host);
        $diff = $collection->diffKeys($rs);
        $rs = $diff->all();

        if ($rs == []) {
            $Words = collect(['0' => trans('multi-lang.nqlinks')]);
            $disabled = "disabled";
        } else {
            $Words = $rs;
            $disabled = "enabled";
        }

        return [
            $Words,
            $disabled
        ];
    }

    // Relaciones
    public function word_question()
    {
        return $this->hasMany('\App\Models\WordQuestion', 'questionId', 'questionId');
    }
    public function question()
    {
        return $this->hasMany('\App\Models\Word', 'orgId', 'orgId');
    }

    public function QuestionLinked()
    {
        return $this->hasMany('\App\Models\QuestionSolution', 'questionId', 'questionId');
    }
    public function WordLinked()
    {
        return $this->hasMany('\App\Models\WordQuestion', 'questionId', 'questionId');
    }
   
    // public function scope()
    // {
    //     return $this->belongsTo('\App\Models\Scope', 'scopeId', 'scope');
    // }
}
