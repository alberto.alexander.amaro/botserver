<?php

/**
 * Application: Spertatek
 * File       : Category.php
 * Type       : Model
 * Function   : Data manipulation for table category
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['categoryName'];
    protected $table = 'category';
    protected $primaryKey = 'categoryId';


    public function getCategoryOrderById()
    {

        return $this->orderBy('categoryId', 'ASC')
            ->lazy();
    }

    public function getCategorySelectById()
    {

        return $this->pluck('categoryName', 'categoryId');
    }

    public function addCategory($fromCont, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $category                = new Category;
            if ($fromCont) {
                $category->categoryName    = $data['serverMemo']['data']['categoryName']; //Usando request en livewire
            } else {
                $category->categoryName  = $data['categoryName']; //Usando request
            }


            $category->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            // $this->emit('row-added',  [, trans('multi-lang.eadded')]);
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function findById($categoryId)
    {
        $rs = $this->where('categoryId', '=', $categoryId)
            ->get();

        return $rs;
    }

    public function updateCategory($fromCont, $id, $data)
    {
        $error = null;
        // dd($data);
        DB::beginTransaction();
        try {

            $category  = Category::find($id);
            if ($fromCont) {
                $category->categoryName    = $data['serverMemo']['data']['categoryName']; //Usando request en livewire
            } else {
                $category->categoryName  = $data['categoryName'];
            }

            $category->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {

            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated'), 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function deleteCategory($id)
    {
        $rs = $this->findOrfail($id);
        if ($rs->word->count() != 0) {
            return $rs = ['class' => 'error', 'alert' =>  trans('multi-lang.enodeleted'), 'message' => trans('multi-lang.nodeleted')];
        } else {
            try {
                $rs->delete();
                $success = true;
            } catch (\Exception $e) {
                $error   = $e->getMessage();
                $success = false;
            }

            if ($success) {
                return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
            } else {
                return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            }
        }
    }

    public function renderLivewire($updateMode, $selected_id, $search, $sort, $direction, $pagination) {
        //         dd(
        //     $updateMode.' '. 
        //     $selected_id.' '. 
        //     $search.' '. 
        //     $sort.' '. 
        //     $direction.' '. 
        //     $pagination
        // );
        if ($updateMode == 1) {
            $data = $this->where('categoryId', '=', $selected_id)
            ->get();
            // dd($data);
        } else {
            if (strlen($search) > 0) {
                $data = $this->where('categoryName', 'LIKE', '%' . $search . '%')
                    ->orderBy($sort, $direction)
                    ->paginate($pagination);
            } else {
                $data = $this->orderBy($sort, $direction)
                    ->paginate($pagination);
            }
            return $data;
        }
    }

    // Relaciones
    public function word()
    {
        return $this->hasMany('\App\Models\Word', 'categoryId', 'categoryId');
    }
}
