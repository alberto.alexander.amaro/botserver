<?php

/**
 * Application: Spertatek
 * File       : Solution.php
 * Type       : Model
 * Function   : Data manipulation for table solution
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 * Version    : 1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Exists;

class Solution extends Model
{

    use HasFactory;
    protected $table = 'solution';
    protected $primaryKey = 'solutionId';
    protected $fillable = [
        'longName',
        'shortName',
        'subject',
        'language',
        'scope',
        'orgId'
    ];


    public function getSolutionOrderById()
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true) {
            return $this
                ->join('organization', 'organization.orgId', 'solution.orgId')
                ->join('scope', 'scope.scopeId', 'solution.scope')
                ->leftJoin('solution_date1', 'solution_date1.parentId', 'solution.solutionId')
                ->leftJoin('solution_date2', 'solution_date2.parentId', 'solution.solutionId')                
                ->orderBy('organization.orgName', 'ASC')
                ->orderBy('shortText', 'ASC')
                ->get();
        } else {
            return
                $this
                ->join('organization', 'organization.orgId', 'solution.orgId')
                ->join('scope', 'scope.scopeId', 'solution.scope')
                ->leftJoin('solution_date1', 'solution_date1.parentId', 'solution.solutionId')
                ->leftJoin('solution_date2', 'solution_date2.parentId', 'solution.solutionId')
                ->orderBy('organization.orgId', 'ASC')
                ->where('solution.orgId', Auth::user()->organization->orgId)
                ->orderBy('shortText', 'ASC')
                ->get();
        }
    }

    public function getLanguage()
    {
        $rs = [
            'en' => 'English',
            'fr' => 'French',
            'es' => 'Spanish'
        ];
        return $rs;
    }

    public function addSolution($data)
    {

        $error = null;

        DB::beginTransaction();
        try {
            // dd($data);
            $solution                = new Solution;
            $solution->introMessage  = $data['introMsg'];
            $solution->longText  = $data['longText'];
            $solution->shortText  = $data['shortText'];
            $solution->subject  = $data['subject'];
            $solution->language  = $data['language'];
            if ($data['url']) {
                $solution->url  = $data['url'];
            }
            if ($data['cost']) {
                $solution->cost  = $data['cost'];
            }
            if ($data['filename']) {
                $solution->filename  = $data['filename'];
            }
            if (($data['filename'])) {
                $solution->filetype  = $data['filetype'];
            }
            $solution->openinChatbot  = $data['openin'];
            $solution->scope  = $data['scope'];
            $solution->orgId  = $data['orgId'];

            $solution->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {

            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added'), 'solutionId' => $solution->solutionId];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function findById($id)
    {
        $rs = $this->select(
            'solution.solutionId',
            'solution.introMessage as introMsg',
            'solution.shortText',
            'solution.longText',
            'solution.subject',
            'solution.language',
            'solution.url',
            'solution.cost',
            'solution.filename',
            'solution.filetype',
            'solution.openinChatbot as openin',
            'solution.scope',
            'solution.orgId',
            'solution_date1.*',
            'solution_date2.*'
        )->leftJoin('solution_date1', 'solution_date1.parentId', 'solution.solutionId')
            ->leftJoin('solution_date2', 'solution_date2.parentId', 'solution.solutionId')
            ->where('solutionId', $id)->get();
        foreach ($rs as $row) {
            $rs = $row;
        }
        // dd($rs);
        // $this->findOrfail($id);
        return $rs;
    }
    public function updateSolution($id, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $solution  = Solution::find($id);
            $solution->introMessage  = $data['introMsg'];
            $solution->longText  = $data['longText'];
            $solution->shortText  = $data['shortText'];
            $solution->subject  = $data['subject'];
            $solution->language  = $data['language'];
            if ($data['url']) {
                $solution->url  = $data['url'];
            }
            if ($data['cost']) {
                $solution->cost  = $data['cost'];
            }
            if ($data['filename']) {
                $solution->filename  = $data['filename'];
            }
            if (($data['filename'])) {
                $solution->filetype  = $data['filetype'];
            }
            $solution->openinChatbot  = $data['openin'];
            $solution->scope  = $data['scope'];
            $solution->orgId  = $data['orgId'];
            $solution->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated') , 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function deleteSolution($id)
    {
        $rs = $this->findOrfail($id);
        if ($rs->hostSolutionLink->count() != 0 or $rs->solutionLinked->count() != 0 or $rs->QuestionSolutionLinked->count() != 0) {
            return $rs = ['class' => 'error', 'alert' =>  trans('multi-lang.enodeleted'), 'message' => trans('multi-lang.nodeleted')];
        } else {
            try {
                $this->where('solutionId', '=', $id)
                    ->delete();

                $success = true;
            } catch (\Exception $e) {
                $error   = $e->getMessage();
                $success = false;
            }

            if ($success) {
                return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
            } else {
                return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            }
        }
    }


    // Comienza la relaciones para crear solution link


    //Funciones para usar controlador de show y edit de solution link
    public function getSolutionSelectShowById($id)
    {
        return $this->where('solutionId', $id)->pluck('shortText', 'solutionId');
    }

    //Funciones para usar controlador de show y edit de solution link
    public function getSolutionSelectAllByIdShow($id)
    {
        $solution = $this->findOrFail($id);
        $rs =  $this->leftJoin('solution_link', 'solution_link.solutionChildId', 'solution.solutionId')->where('solutionParentId', '=', $id)->pluck('shortText', 'solutionId');
        $host = $this->where('solutionId', '<>', $id)->where('orgId', '=', $solution->orgId)->where('scope', '=', $solution->scope)->pluck('shortText', 'solutionId');
        $collection = collect($host);
        $diff = $collection->diffKeys($rs);
        $rs = $diff->all();

        if ($rs == []) {
            $organization = collect(['0' => trans('multi-lang.nlsolutions')]);
            $disabled = "disabled";
        } else {
            $organization = $rs;
            $disabled = "enabled";
        }

        return [
            $organization,
            $disabled
        ];
    }




    public function solution()
    {
        return $this->hasMany('\App\Models\Organization', 'orgId', 'orgId');
    }

    public function hostSolutionLink()
    {
        return $this->hasMany('\App\Models\SolutionLink', 'solutionParentId', 'solutionId');
    }
    public function solutionLinked()
    {
        return $this->hasMany('\App\Models\SolutionLink', 'solutionChildId', 'solutionId');
    }

    public function QuestionSolutionLinked()
    {
        return $this->hasMany('\App\Models\QuestionSolution', 'solutionId', 'solutionId');
    }
}
