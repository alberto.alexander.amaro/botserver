<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Scope extends Model
{
    use HasFactory;
    protected $table = 'scope';
    protected $primaryKey = 'scopeId';
    protected $fillable = [
        'scopeName',
    ];

   public function getScopeId()
   {
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true || Auth::user()->organization->orgIsDefault == 1 ) {
            $rs = $this->select('scopeId as id', 'scopeName as name')->get();
           
        } else {
             $rs = $rs = $this->select('scopeId as id', 'scopeName as name')->where('scopeId', '!=', 0)->get();
        }

        return $rs;
    }
    
   }
//    public function scope()
//    {
//        return $this->belongsTo('\App\Models\Question', 'scope', 'scopeId');
//    }

}
