<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SolutionLink extends Model
{
    use HasFactory;
    protected $table = 'solution_link';
    protected $primaryKey = 'solutionLinkId';
    protected $fillable = [
        'solutionParentId',
        'solutionChildId'
    ];

    public function getSolutionOrderSuperById($id)
    {
                $rs = $this
                ->join('solution', 'solution.solutionId', 'solution_link.solutionParentId')
                ->join('organization', 'organization.orgId', 'solution.orgId')
                ->join('scope', 'scope.scopeId', 'solution.scope')
                ->where('solutionParentId', $id)
                ->get();

            return $rs;
                

    }

    public function addSolutionLink($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $solutionlink                = new SolutionLink;
            $solutionlink->solutionParentId  = $data['hostId'];
            $solutionlink->solutionChildId  = $data['memberId'];

            $solutionlink->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }


    public function deleteSolutionLink($id)
    {

        $rs = $this->findOrfail($id);

        try {
            $rs->delete();
            $success = true;
        } catch (\Exception $e) {
            $error   = $e->getMessage();
            $success = false;
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    //   Relaciones
    
    public function solutionLinked()
    {
        return $this->belongsTo('\App\Models\Solution', 'solutionChildId', 'solutionId');
    }

    public function hostSolutionLink()
    {
        return $this->belongsTo('\App\Models\Solution', 'solutionParentId', 'solutionId');
    }

}
