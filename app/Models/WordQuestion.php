<?php

/**
 * Application: Spertatek
 * File       : WordQuestion.php
 * Type       : Model
 * Function   : Data manipulation for table word_question
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WordQuestion extends Model
{
    use HasFactory;
    protected $table = 'word_question';
    protected $primaryKey = 'wqId';
    protected $fillable = [
        'wordId',
        'questionId'
    ];


    public function deleteQuestionLink($id)
    {
        // dd($id);
        try {
            DB::table('word_question')->where('wordId', '=', $id)->delete();
            $success = true;
        } catch (\Exception $e) {
            $error   = $e->getMessage();
            $success = false;
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function getQuestionLinkById($id)
    {

        $rs = $this::join('question', 'question.questionId', 'word_question.questionId')
            ->join('word', 'word_question.wordId', 'word.wordId')
            ->where('question.questionId', $id)->get();

        return $rs;
    }

    public function addLinkQuestion($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $wordQuestion                = new WordQuestion;
            $wordQuestion->wordId  = $data['wordId'];
            $wordQuestion->questionId  = $data['questionId'];
            $wordQuestion->save();
            
            $success = true;
            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    //   Relaciones



}
