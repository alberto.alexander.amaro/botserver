<?php

/**
 * Application: Spertatek
 * File       : Word.php
 * Type       : model
 * Function   : Data manipulation for table word
 * Developer  : Alberto Amaro   
 * Updated    : 07 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Word extends Model
{
    use HasFactory;
    protected $table = 'word';
    protected $primaryKey = 'wordId';
    protected $fillable = [
        'wordName',
        'categoryId',
        'scope',
        'orgId'
    ];

    public function getWordOrderById()
    {

        if (Auth::user()->hasRole('Superuser') != true && Auth::user()->hasRole('Specialuser') != true) {
            return $this->join('organization', 'organization.orgId', 'word.orgId')->join('scope', 'scope.scopeId', 'word.scope')->where('word.orgId', Auth::user()->organization->orgId)->orderBy('wordId', 'ASC')->get();
        } else {
            return $this->join('organization', 'organization.orgId', 'word.orgId')->join('scope', 'scope.scopeId', 'word.scope')->orderBy('wordId', 'ASC')->get();
        }
    }

    public function addWord($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $word                = new Word;
            $word->wordName  = $data['wordName'];
            $word->categoryId  = $data['categoryId'];
            $word->scope  = $data['scope'];
            $word->orgId  = $data['orgId'];

            $word->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }


    public function findById($id)
    {
        $rs = $this->findOrfail($id);
        return $rs;
    }

    public function updateWord($id, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $word  = Word::find($id);
            $word->wordName  = $data['wordName'];
            $word->categoryId  = $data['categoryId'];
            $word->scope  = $data['scope'];
            $word->orgId  = $data['orgId'];

            $word->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated') , 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function deleteWord($id)
    {
        $rs = $this->findOrfail($id);
        if ($rs->word_question->count() != 0) {
            return $rs = ['class' => 'error', 'alert' =>  trans('multi-lang.enodeleted'), 'message' => trans('multi-lang.nodeleted')];
        } else {
            try {
                $rs->delete();
                $success = true;
            } catch (\Exception $e) {
                $error   = $e->getMessage();
                $success = false;
            }
            if ($success) {
                return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
            } else {
                return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            }
        }
    }

    public function getWordSelectQuestionLinkByName()
    {
        if (Auth::user()->hasRole('Superuser') == true or Auth::user()->hasRole('Specialuser') == true) {
            $Words = Word::leftJoin('word_question', 'word.wordId', 'word_question.wordId')
                ->get(['wordName', 'word.wordId', 'word_question.questionId']);
            $rs =  $Words->where('questionId', '==', null)->sortBy('wordName');
            $rs = $rs->pluck('wordName', 'wordId');
        } else {
            $Words = Word::leftJoin('word_question', 'word.wordId', 'word_question.wordId')
                ->where('word.orgId', Auth::user()->organization->orgId)
                ->get(['wordName', 'word.wordId', 'word_question.questionId']);
            $rs =  $Words->where('questionId', '==', null)->sortBy('wordName');
            $rs = $rs->pluck('wordName', 'wordId');
        }

        if ($rs->count() == 0) {
            $Words = collect(['0' => trans('multi-lang.nwlinks')]);
            $disabled = "disabled";
        } else {
            $Words = $rs;
            $disabled = "enabled";
        }

        return [
            $Words,
            $disabled
        ];
    }
    //   Relaciones
    public function word_question()
    {
        return $this->hasMany('\App\Models\WordQuestion', 'wordId', 'wordId');
    }
    public function word()
    {
        return $this->hasMany('\App\Models\Category', 'categoryId', 'categoryId');
    }

    // public function scope()
    // {
    //     return $this->hasMany('\App\Models\Scope', 'scopeId', 'scope');
    // }

    public function organization()
    {
        return $this->hasMany('\App\Models\Organization', 'orgId', 'orgId');
    }
}
