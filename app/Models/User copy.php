<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    public function getUsersById()
    {
        if (Auth::user()->hasRole('Superuser') == true) {
            $users = User::all();
        } else {
            if (Auth::user()->hasRole('Specialuser') == true) {
                $result =  User::all();
                foreach ($result as $user) {
                    foreach ($user->getRoleNames() as $role) {
                        if ($role != 'Superuser') {
                            $users[] = $user;
                        }
                    }
                }
            } else {
                if (Auth::user()->hasRole('Manager') == true) {
                    $result = User::where('orgId', Auth::user()->organization->orgId)->get();
                    foreach ($result as $user) {
                        foreach ($user->getRoleNames() as $role) {
                            if ($role != 'Superuser' && $role != 'Specialuser') {
                                $users[] = $user;
                            }
                        }
                    }
                } else {
                    $result = User::where('orgId', Auth::user()->organization->orgId)->get();
                    foreach ($result as $user) {
                        foreach ($user->getRoleNames() as $role) {
                            if ($role != 'Superuser' && $role != 'Specialuser' && $role != 'Manager') {
                                $users[] = $user;
                            }
                        }
                    }
                }
            }
        }
        return $users;
    }
    public function findById($userId)
    {
        $rs = $this->findOrfail($userId);
        return $rs;
    }
    public function getUsersForEditById($userId)
    {
        $res = $this->select('id', 'name as userName', 'email as userEmail', 'orgId as userOrgnizationId', 'isActive as userActive')->where('id', $userId)->get();
        foreach ($res as $r) {
            $rs = $r;
        }

        return $rs;
    }

    public function getUserSession()
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true || Auth::user()->organization->orgIsDefault == 1) {
            // if (Auth::user()->hasRole('Superuser') == true or Auth::user()->hasRole('Specialuser') == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getUsersRadioById()
    {
        $result = Role::all();

        if (Auth::user()->hasRole('Superuser') == true) {
            $roles = $result->sortBy('name');
        } else {
            if (Auth::user()->hasRole('Specialuser') == true) {
                $roles =  $result->where('name', '<>', 'Superuser')->sortBy('name');
            } else {
                if (Auth::user()->can('Manager') == true) {
                    $roles =  $result->where('name', '!=', 'Specialuser')->where('name', '<>', 'Superuser')->sortBy('name');
                } else {
                    $roles =  $result->where('name', '!=', 'Specialuser')->where('name', '<>', 'Superuser')->where('name', '<>', 'Manager')->sortBy('name');
                }
            }
        }

        return $roles;
    }

    public function addUser($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $user                = new User;
            $user->name  = $data['userName'];
            $user->email  = $data['userEmail'];
            $user->password  = bcrypt($data['userEmail']);
            $user->orgId  = $data['userOrgnizationId'];
            $user->isActive  = $data['userActive'];

            $user->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            $user->roles()->sync($data['role']);
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded'), 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function updateUser($id, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $user  = User::find($id);
            $user->name  = $data['userName'];
            $user->email  = $data['userEmail'];
            $user->orgId  = $data['userOrgnizationId'];
            $user->isActive  = $data['userActive'];
            $user->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            $user->roles()->sync($data['role']);
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated'), 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */

    protected $fillable = [
        'name',
        'email',
        'password',
        'orgId', /*Uso de API*/
        'isActive' /*Uso de API*/
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
    // Relacion uno a muchos
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function organization()
    {
        return $this->belongsTo('\App\Models\Organization', 'orgId', 'orgId');
    }
    public function question()
    {
        return $this->hasMany('\App\Models\Question', 'orgId', 'orgId');
    }
    public function chats()
    {
        return $this->belongsToMany('App\Models\Chat');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }
}
