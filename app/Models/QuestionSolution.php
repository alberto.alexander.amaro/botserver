<?php
/**
 * Application: Spertatek
 * File       : QuestionSolution.php
 * Type       : Model
 * Function   : Data manipulation for table question_solution
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuestionSolution extends Model
{
    use HasFactory;
    protected $table = 'question_solution';
    protected $primaryKey = 'qsId';
    protected $fillable = [
        'questionId',
        'solutionId'
        
    ];
    public function deleteQuestionLink($id)
    {
        try {
            DB::table('question_solution')->where('questionId', '=', $id)->delete();
            $success = true;
        } catch (\Exception $e) {
            $error   = $e->getMessage();
            $success = false;
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function getQuestionLinkById($id)
    {

        $rs = $this::join('solution', 'solution.solutionId', 'question_solution.solutionId')
            ->join('question', 'question_solution.questionId', 'question.questionId')
            ->where('solution.solutionId', $id)->get();
        return $rs;
    }

    public function addLinkQuestion($data)
    {
       
        $error = null;

        DB::beginTransaction();
        try {

            $questionSolution                = new QuestionSolution;
            $questionSolution->questionId  = $data['wordId'];
            $questionSolution->solutionId  = $data['questionId'];

            $questionSolution->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function QuestionLinked()
    {
        return $this->belongsTo('\App\Models\Question', 'questionId', 'questionId');
    }

    public function hostSolutionLink()
    {
        return $this->belongsTo('\App\Models\Solution', 'solutionParentId', 'solutionId');
    }

}
