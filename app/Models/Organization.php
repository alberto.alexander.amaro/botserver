<?php

/**
 * Application: Spertatek
 * File       : Organization.php
 * Type       : Model
 * Function   : Data manipulation for table Organization
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Organization extends Model
{
    use HasFactory;
    protected $table = 'organization';
    protected $primaryKey = 'orgId';

    protected $fillable = ['orgName', 'orgContact', 'orgEmail', 'orgLanguage', 'orgIsDefault', 'orgIsActive'];



    public function getOrganizationOrderById()
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true) {
            return $this->orderBy('orgName', 'ASC')->get();
        } else {
            return $this->where('orgId', Auth::user()->organization->orgId)->get();
        }
    }

    public function getOrganizationSelectAllById()
    {
        if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true) {
            $rs = $this->pluck('orgName', 'orgId');
        } else {
            $rs =  $this->leftJoin('organization_group', 'organization_group.organizationId', 'organization.orgId')->where('hostOrganizationGroupId', '=', Auth::user()->organization->orgId)->pluck('orgName', 'orgId');
            $host = $this->where('orgId', '<>', Auth::user()->organization->orgId)->pluck('orgName', 'orgId');
            $collection = collect($host);
            $diff = $collection->diffKeys($rs);
            $rs = $diff->all();
        }
        if ($rs == []) {
            $organization = collect(['0' => 'No organizations available']);
            $disabled = "disabled";
        } else {
            $organization = $rs;
            $disabled = "enabled";
        }

        return [
            $organization,
            $disabled
        ];
    }

    public function getOrganizationSelectById()
    {
        if (Auth::user()->hasRole('Superuser') == true) {
            return $this->pluck('orgName', 'orgId');
        } else {
            if (Auth::user()->hasRole('Specialuser') == true) {
                return $this->pluck('orgName', 'orgId');
            } else {
                return $this->where('orgId', Auth::user()->organization->orgId)->pluck('orgName', 'orgId');
            }
        }
    }


    //Funciones para usar controlador de show y edit de organization group
    public function getOrganizationSelectShowById($id) 
    {
        return $this->where('orgId', $id)->pluck('orgName', 'orgId');
    }

    //Funciones para usar controlador de show y edit de organization group
    public function getOrganizationSelectAllByIdShow($id)
    {
        $rs =  $this->leftJoin('organization_group', 'organization_group.organizationId', 'organization.orgId')->where('hostOrganizationGroupId', '=', $id)->pluck('orgName', 'orgId');
        $host = $this->where('orgId', '<>', $id)->pluck('orgName', 'orgId');
        $collection = collect($host);
        $diff = $collection->diffKeys($rs);
        $rs = $diff->all();

        if ($rs == []) {
            $organization = collect(['0' => 'No organizations available']);
            $disabled = "disabled";
        } else {
            $organization = $rs;
            $disabled = "enabled";
        }

        return [
            $organization,
            $disabled
        ];
    }

    public function getOrgIsDefault()
    {
        $rs = $this->where('orgIsDefault', '=', '1')->get();

        if ($rs->count() != 0) {
            $rs = '1';
        } else {
            $rs = '0';
        }
        
        return $rs;
    }
    public function getOrgIsLanguage()
    {
        $rs = [
            'en' => 'English',
            'fr' => 'French',
            'es' => 'Spanish'
        ];
        return $rs;
    }
    public function addOrganization($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $organization                = new Organization;
            $organization->orgName  = $data['orgName'];
            $organization->orgContact  = $data['orgContact'];
            $organization->orgEmail  = $data['orgEmail'];
            $organization->orgLanguage  = $data['orgLanguage'];
            $organization->orgMultilanguage  = $data['orgMultilanguage'];
            $organization->orgKaas  = $data['orgKaas'];
            $organization->orgRPA  = $data['orgRPA'];
            $organization->orgIsDefault  = $data['orgIsDefault'];
            $organization->orgIsActive  = $data['orgIsActive'];

            $organization->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function findById($id)
    {
        $rs = $this->findOrfail($id);
        return $rs;
    }
    public function updateOrganization($id, $data)
    {
        $error = null;

        DB::beginTransaction();
        try {

            $organization  = Organization::find($id);
            $organization->orgName  = $data['orgName'];
            $organization->orgContact  = $data['orgContact'];
            $organization->orgEmail  = $data['orgEmail'];
            $organization->orgLanguage  = $data['orgLanguage'];
            $organization->orgMultilanguage  = $data['orgMultilanguage'];
            $organization->orgKaas  = $data['orgKaas'];
            $organization->orgRPA  = $data['orgRPA'];
            $organization->orgIsDefault  = $data['orgIsDefault'];
            $organization->orgIsActive  = $data['orgIsActive'];

            $organization->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eupdated') , 'message' => trans('multi-lang.updated')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function deleteOrganization($id)
    {

        $rs = $this->findOrfail($id);
        if ($rs->organization->count() != 0 or $rs->question->count() != 0 or $rs->solution->count() != 0) {
            return $rs = ['class' => 'error', 'alert' =>  trans('multi-lang.enodeleted'), 'message' => trans('multi-lang.nodeleted')];
        } else {
            try {
                $rs->delete();
                $success = true;
            } catch (\Exception $e) {
                $error   = $e->getMessage();
                $success = false;
            }

            if ($success) {
                return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
            } else {
                return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            }
        }
    }
    // Relaciones
    public function organization()
    {
        return $this->hasMany('\App\Models\Word', 'orgId', 'orgId');
    }

    public function hostOrganizationGroup()
    {
        return $this->hasMany('\App\Models\OrganizationGroup', 'hostOrganizationGroupId', 'orgId');
    }

    public function question()
    {
        return $this->hasMany('\App\Models\Question', 'orgId', 'orgId');
    }
    public function solution()
    {
        return $this->hasMany('\App\Models\Solution', 'orgId', 'orgId');
    }
}
