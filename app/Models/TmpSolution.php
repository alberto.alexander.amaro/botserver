<?php
/**
 * Application: Spertatek
 * File       : TmpSolution.php
 * Type       : Model
 * Function   : Data manipulation for table tmp_solution
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpSolution extends Model
{
    use HasFactory;
    protected $table = 'tmp_solution';
    protected $primaryKey = 'tmpSolutionId';
    protected $fillable = [
        'solutionId',
        'longName',
        'shortName',
        'subject',
        'language',
        'scope',
        'orgId'
    ];
}
