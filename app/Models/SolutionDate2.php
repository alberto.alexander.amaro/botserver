<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SolutionDate2 extends Model
{
    use HasFactory;
    protected $table = 'solution_date2';
    protected $primaryKey = 'solutionDate2Id';
    protected $fillable = [
        'parentId',
        'date2'
    ];

    public function addDate2($parentId, $date2)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $date                = new SolutionDate2;
            $date->parentId  = $parentId;
            $date->date2  = $date2;


            $date->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }
        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
            
        }
    }

    public function updateDate2($parentId, $date2)
    {

        $error = null;

        DB::beginTransaction();
        try {
            $date  = SolutionDate2::where('parentId', $parentId)->get();
            if ($date->count() != 0) {
                foreach ($date as $date) {
                    $date = $date;
                }
            } else {
                $date                = new SolutionDate2;
                $date->parentId  = $parentId;
            }

            $date->date2  = $date2;
            $date->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }
        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }
    public function solutionDate1()
    {
        return $this->belongsTo('\App\Models\Solution', 'solutionId', 'parentId');
    }
}

