<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Scope extends Model
{
    use HasFactory;
    public function getScopeId()
    {
        {
            if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true || Auth::user()->organization->orgIsDefault == 1 ) {
                $rs = [
                    '0' => ['scope' => '0',  'name' => 'Public', 'selected' => true],
                    '1' => ['scope' => '1',  'name' => 'Private', 'selected' => false],
                    '2' => ['scope' => '2',  'name' => 'Group', 'selected' => false]
                ];
               
            } else {
                 $rs = [
                    '0' => ['id' => '1',  'name' => 'Private', 'selected' => true],
                    '1' => ['id' => '2',  'name' => 'Group', 'selected' => false]
                ];
            }
    
            return $rs;
        }
    }
}
