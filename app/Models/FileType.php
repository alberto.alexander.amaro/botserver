<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    use HasFactory;
    public function getFileType()
    {
        {
            
                 $rs = [

                    '0' => ['id' => 'jpg',  'name' => 'JPG', 'selected' => false],
                    '1' => ['id' => 'mp3',  'name' => 'MP3', 'selected' => false],
                    '2' => ['id' => 'mp4',  'name' => 'MP4', 'selected' => false],
                    '3' => ['id' => 'pdf',  'name' => 'PDF', 'selected' => false]
                ];
    
            return $rs;
        }
    }
}
