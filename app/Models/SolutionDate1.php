<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SolutionDate1 extends Model
{
    use HasFactory;
    protected $table = 'solution_date1';
    protected $primaryKey = 'solutionDate1Id';
    protected $fillable = [
        'parentId',
        'date1'
    ];

    public function addDate1($parentId, $date1)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $date                = new SolutionDate1;
            $date->parentId  = $parentId;
            $date->date1  = $date1;


            $date->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }
        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function updateDate1($parentId, $date1)
    {

        $error = null;

        DB::beginTransaction();
        try {
            $date  = SolutionDate1::where('parentId', $parentId)->get();
            if ($date->count() != 0) {
                foreach ($date as $date) {
                    $date = $date;
                }
            } else {
                $date                = new SolutionDate1;
                $date->parentId  = $parentId;
            }

            $date->date1  = $date1;
            $date->save();
            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }
        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function solutionDate1()
    {
        return $this->belongsTo('\App\Models\Solution', 'solutionId', 'parentId');
    }
}
