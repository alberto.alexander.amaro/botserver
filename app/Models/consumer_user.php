<?php
/**
 * Application: Spertatek
 * File       : consumer_user.php
 * Type       : Model
 * Function   : Data manipulation for table consumer_user
 * Developer  : Alberto Amaro   
 * Updated    : 14 de junio 2022
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class consumer_user extends Model
{
    use HasFactory;
}
