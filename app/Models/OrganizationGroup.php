<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrganizationGroup extends Model
{
    use HasFactory;
    protected $table = 'organization_group';
    protected $primaryKey = 'organizationGroupId';
    protected $fillable = [
        'hostOrganizationGroupId',
        'organizationId'
    ];
    public function getOrganizationOrderById()
    {
        // if (Auth::user()->hasRole('Superuser') == true || Auth::user()->hasRole('Specialuser') == true) {
        //     return $this
        //         ->join('organization', 'organization.orgId', 'organization_group.hostOrganizationGroupId')
        //         ->orderBy('orgName', 'ASC')->get();
        // } else {
        //     return $this
        //         ->join('organization', 'organization.orgId', 'organization_group.hostOrganizationGroupId')
        //         ->where('hostOrganizationGroupId', Auth::user()->organization->orgId)->get();
        // }
    }

///Esto es para lo de super
    public function getOrganizationOrderSuperById($id)
    {
            return $this
                ->join('organization', 'organization.orgId', 'organization_group.hostOrganizationGroupId')
                ->where('hostOrganizationGroupId', $id)->get();
    }

    public function addOrganizationGroup($data)
    {

        $error = null;

        DB::beginTransaction();
        try {

            $organizationGroup                = new OrganizationGroup;
            $organizationGroup->hostOrganizationGroupId  = $data['hostId'];
            $organizationGroup->organizationId  = $data['memberId'];

            $organizationGroup->save();

            $success = true;
            DB::commit();
        } catch (\Exception $e) {

            $success = false;
            $error   = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            return $rs = $rs = ['class' => 'success', 'alert' => trans('multi-lang.eadded') , 'message' => trans('multi-lang.added')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }


    public function deleteOrganizationGroup($id)
    {

        $rs = $this->findOrfail($id);

        try {
            $rs->delete();
            $success = true;
        } catch (\Exception $e) {
            $error   = $e->getMessage();
            $success = false;
        }

        if ($success) {
            return $rs = ['class' => 'success', 'alert' => trans('multi-lang.edeleted') , 'message' => trans('multi-lang.deleted')];
        } else {
            return $rs = ['class' => 'danger', 'alert' => 'Error: ', 'message' => $error];
        }
    }

    public function hostOrganizationGroup()
    {
        return $this->belongsTo('\App\Models\Organization', 'organizationId', 'orgId');
    }
}
