<?php

/**
 * Application: Spertatek
 * File       : SolutionRequest.php
 * Type       : Controller request
 * Function   : Data manipulation for form add and edit solution
 * Developer  : Alberto Amaro   
 * Updated    : 07 de junio 2022
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\Organization;
use App\Models\User;


class SolutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private $oUser;


    public function __construct()
    {
        $this->oUser = new User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $sessionUser = $this->oUser->getUserSession();
        $organization = Organization::find($this->orgId);
        $solution = $this->route()->parameter('solution');

        $rules = [
            // 'introMsg' => [
            //     'required',
            //     'min:3',
            // ], No es requerido
            'longText' => [
                'required',
                'min:3',
            ],
            'shortText' => [
                'required',
                'min:3',
            ],
            'subject' => [
                'required',
                'min:3',
            ],
            'openin' => [
                'required',
                'in:y,n',
            ],
            'orgId' => [
                'required',
                'numeric',
                Rule::exists('organization', 'orgId', $this->orgId),
            ],
            'language' => 'required|in:en,fr,es',
            'scope' => 'required|integer|in:0,1,2',
        ]; //Registro y edicion de word con scope private y orgId diferente
        if ($this->introMsg <> '') {
            $rules = array_merge($rules, [
                'introMsg' => 'min:3|max:150',
            ]);
        }
        if ($this->url <> '') {
            $rules = array_merge($rules, [
                'url' => 'url',
            ]);
        }
        if ($this->cost <> '') {
            $rules = array_merge($rules, [
                'cost' => 'numeric',
            ]);
        }

        if ($this->filename <> '') {
            $rules = array_merge($rules, [
                'filetype' => 'required|in:jpg,mp3,mp4,pdf'
            ]);
        }
        if ($this->date1 <> '') {
            $rules = array_merge($rules, [
                'date1' => 'date'
            ]);
        }
        if ($this->date2 <> '') {
            $rules = array_merge($rules, [
                'date2' => 'date'
            ]);
        }

        if ($this->scope == 1) {
            $rules = array_merge($rules, [
                // 'introMsg' => [
                //     'required',
                //     'min:3',
                //     Rule::unique('solution', 'introMessage')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'), //resgistro                                     
                // ], No es requerido
                'longText' => [
                    'required',
                    'min:3',
                    Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'), //resgistro                                     
                ],
                'shortText' => [
                    'required',
                    'min:3',
                    Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'), //resgistro                                     
                ],
                'subject' => [
                    'required',
                    'min:3',
                    Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'), //resgistro                                     
                ],
            ]); //Registro y edicion de word con scope private y orgId diferente
        } else {
            if ($organization) {
                if ($organization->orgIsDefault == 1 and $this->scope == 0) { //Auth::user()->organization->orgIsDefault == 1
                    $rules = array_merge($rules, [
                        // 'introMsg' => [
                        //     'required',
                        //     'min:3',
                        //     Rule::unique('solution', 'introMessage')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'),
                        // ], No es requerido
                        'longText' => [
                            'required',
                            'min:3',
                            Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'),
                        ],
                        'shortText' => [
                            'required',
                            'min:3',
                            Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'),
                        ],
                        'subject' => [
                            'required',
                            'min:3',
                            Rule::unique('solution')->where('orgId', $this->orgId)->ignore($solution, 'solutionId'),
                        ],
                    ]); ////Registro y edicion de word con scope public de organization por default.
                } else {
                    if ($organization->orgIsDefault != 1 and $this->scope == 0) {
                        $rules = array_merge($rules, [
                            'scope' => 'required|integer|between:1,2'
                        ]);

                        ////Evitar registro y edicion de word con scope public con organization sin default.
                    } else {
                        $rules = array_merge($rules, [
                            // 'introMsg' => 'required|min:3|unique:solution,introMessage,' . $solution . ',solutionId', No es requerido
                            'longText' => 'required|min:3|unique:solution,longText,' . $solution . ',solutionId',
                            'shortText' => 'required|min:3|unique:solution,shortText,' . $solution . ',solutionId',
                            'subject' => 'required|min:3|unique:solution,subject,' . $solution . ',solutionId',

                        ]); ////Registro y edicion de word con scope group.
                    }
                }
            }
        }
        if ($sessionUser == false) {
            $rules = array_merge($rules, [
                'orgId' => [
                    'required',
                    'numeric',
                    'in:' . Auth::user()->organization->orgId,
                ],
                'scope' => 'required|integer|in:1,2'
            ]);
        }

        return $rules;
    }
}
