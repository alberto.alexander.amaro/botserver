<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    private $oUser;

    public function __construct()
    {
        $this->oUser = new User;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = $this->oUser->getUsersRadioById();
        $role = $roles->find($this->role);

        $sessionUser = $this->oUser->getUserSession();

        $user = $this->route()->parameter('user');

        $rules = [
            'userName' => 'required|min:3|unique:organization,orgName,' . $this->userOrgnizationId . ',orgId',
            'userEmail' => 'required|min:3|email|unique:users,email,' . $user . ',id',
            'userOrgnizationId' => [
                'required',
                'numeric',
                Rule::exists('organization', 'orgId', $this->userOrgnizationId),
            ],
            'role' => [
                'required',
                'numeric',
                Rule::exists('roles', 'id', $this->role),
            ],
            'userActive' => 'required|integer|in:0,1',
        ];
        if ($sessionUser == false) {
            $rules = array_merge($rules, [
                'userOrgnizationId' => [
                    'required',
                    'numeric',
                    'in:' . Auth::user()->organization->orgId,
                ],
            ]);
        }
        if (!$role) {
            $rules = array_merge($rules, [
                'role' => [
                    'required',
                    'numeric',
                    'not_in:' . $this->role,
                ],
            ]);
        }

        return $rules;
    }
}
