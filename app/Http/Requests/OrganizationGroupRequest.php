<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrganizationGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'hostId' => [
                'required',
                'numeric',
                Rule::exists('organization', 'orgId', $this->hostId ),                              
            ],
            'memberId' => [
                'numeric',
                'different:hostId',
                'not_in:0',
                Rule::unique('organization_group', 'organizationId')->where('hostOrganizationGroupId', $this->hostId),//resgistro     
                Rule::exists('organization', 'orgId', $this->memberId ),                       
            ],           
        ];
        return $rules;
    }
}
