<?php

/**
 * Application: Spertatek
 * File       : CategoryRequest.php
 * Type       : Controller request
 * Function   : Data manipulation for form add and edit category
 * Developer  : Alberto Amaro   
 * Updated    : 07 de junio 2022
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $category = $this->route()->parameter('category');

        $rules = [
            'categoryName' => 'required|min:1|unique:category,categoryName,' . $category . ',categoryId'            
        ];
        return $rules;
    }
}
