<?php

/**
 * Application: Spertatek
 * File       : QuestionRequest.php
 * Type       : Controller request
 * Function   : Data manipulation for form add and edit question
 * Developer  : Alberto Amaro   
 * Updated    : 07 de junio 2022
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\Organization;
use App\Models\User;


class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    private $oUser;


    public function __construct()
    {
        $this->oUser = new User;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    // public function rules()
    // {
    //     $question = $this->route()->parameter('question');

    //     $rules = [
    //         'questionName' => 'required|min:3|unique:question,questionName,' . $question . ',questionId',
    //         'scope' => 'required|integer|in:0,1,2',
    //         'orgId' => 'required|numeric|exists:organization'
    //     ];

    //     return $rules;
    // }
    public function rules()
    {
        $sessionUser = $this->oUser->getUserSession();
        $organization = Organization::find($this->orgId);
        $question = $this->route()->parameter('question');

        $rules = [
            'questionName' => [
                'required',
                'min:3',
            ],
            'orgId' => [
                'required',
                'numeric',
                Rule::exists('organization', 'orgId', $this->orgId),
            ],
            'scope' => 'required|integer|in:0,1,2',
        ]; //Registro y edicion de word con scope private y orgId diferente

        if ($this->scope == 1) {
            $rules = array_merge($rules, [
                'questionName' => [
                    'required',
                    'min:3',
                    Rule::unique('question')->where('orgId', $this->orgId)->ignore($question, 'questionId'), //resgistro                                     
                ],
            ]); //Registro y edicion de word con scope private y orgId diferente
        } else {
            if ($organization) {
                if ($organization->orgIsDefault == 1 and $this->scope == 0) { //Auth::user()->organization->orgIsDefault == 1
                    $rules = array_merge($rules, [
                        'questionName' => [
                            'required',
                            'min:3',
                            Rule::unique('question')->where('orgId', $this->orgId)->ignore($question, 'questionId'),
                        ],
                    ]); ////Registro y edicion de word con scope public de organization por default.
                } else {
                    if ($organization->orgIsDefault != 1 and $this->scope == 0) {
                        $rules = array_merge($rules, [
                            'scope' => 'required|integer|between:1,2'
                        ]);

                        ////Evitar registro y edicion de word con scope public con organization sin default.
                    } else {
                        $rules = array_merge($rules, [
                            'questionName' => 'required|min:3|unique:question,questionName,' . $question . ',questionId'
                            
                        ]); ////Registro y edicion de word con scope group.
                    }
                }
            }
        }
        if ($sessionUser == false) {
            $rules = array_merge($rules, [
                'orgId' => [
                    'required',
                    'numeric',
                    'in:' . Auth::user()->organization->orgId,
                ],
                'scope' => 'required|integer|in:1,2'
            ]);
        }

        return $rules;
    }
  
}
