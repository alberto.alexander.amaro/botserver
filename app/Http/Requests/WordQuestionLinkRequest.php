<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WordQuestionLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $word = $this->route()->parameter('word');
        $wr = $this->request->all();

        $rules = [
            'wordId' => 'required|numeric|not_in:0|exists:word|unique:word_question',
        ];

        return $rules;
    }
}
