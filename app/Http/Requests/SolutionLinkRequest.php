<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SolutionLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'hostId' => [
                'required',
                'numeric',
                Rule::exists('solution', 'solutionId', $this->hostId ),                              
            ],
            'memberId' => [
                'numeric',
                'different:hostId',
                'not_in:0',
                Rule::unique('solution_link', 'solutionChildId')->where('solutionParentId', $this->hostId),//resgistro     
                Rule::exists('solution', 'solutionId', $this->memberId ),                       
            ],           
        ];
        return $rules;
    }
}
