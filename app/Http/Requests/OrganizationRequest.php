<?php

/**
 * Application: Spertatek
 * File       : OrganizationRequest.php
 * Type       : Controller request
 * Function   : Data manipulation for form add and edit organization
 * Developer  : Alberto Amaro   
 * Updated    : 07 de junio 2022
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    private $oUser;


    public function __construct()
    {
        $this->oUser = new User;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $sessionUser = $this->oUser->getUserSession();
        // dd($sessionUser);
        $organization = $this->route()->parameter('organization');
        $rules = [
            'orgName' => 'required|min:3|unique:organization,orgName,' . $organization . ',orgId',
            'orgContact' => 'required|min:3|unique:organization,orgContact,' . $organization . ',orgId',
            'orgEmail' => 'required|min:3|email|unique:organization,orgEmail,' . $organization . ',orgId',
            'orgLanguage' => 'required|in:en,fr,es',
            'orgIsActive' => 'required|integer|in:0,1',
        ];
        
        if ($sessionUser) {
            $rules = array_merge($rules, [
                'orgIsDefault' => 'required|integer|in:0,1',
            ]);
        }else{
            $rules = array_merge($rules, [
                'orgIsDefault' => 'required|integer|in:0',
            ]);
            
        }
        // dd($rules);
        return $rules;
    }
}
