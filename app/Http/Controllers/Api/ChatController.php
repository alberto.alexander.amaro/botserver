<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat;

class ChatController extends Controller
{
    public function __construct(){
        $this->middleware('auth:sanctum');
    }
    public function userChats($res) {
        return Chat::orWhereRaw("JSON_CONTAINS(members, ?)", $res)->get();
            // return Chat::whereJsonContains('members', $res)->get();
      }
}
