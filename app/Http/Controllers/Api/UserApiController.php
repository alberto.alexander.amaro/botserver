<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Carbon;

use Illuminate\Validation\ValidationException;

class UserApiController extends Controller
{
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        // $this->guard()->login($user);
        return response()->json([
            'user' => $user,
            'message' => 'registration successful'
        ], 200);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'password' => ['required', 'string', 'min:4', 'confirmed'],
            // NO PASSWORD CONFIRMATION
            'password' => ['required', 'string', 'min:4'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'followers' => [],
            'following' => [],
            'password' => Hash::make($data['password']),
        ]);
    }
    protected function guard()
    {
        return Auth::guard();
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // return response()->json(['message' => $request->all()], 500);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $authuser = auth()->user();
            $user = User::where('email', $request->email)->first();

            if (!$user || !Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                    'email' => ['The provided credentials are incorrect.'],
                ]);
            }

            // return $user->createToken($request->device_name);


            $token = $user->createToken('Prueba Personal Access Token', ['*'], Carbon::now()->addMinutes(10))->plainTextToken;
            // if (!$user->remember_token) {
            //     $token->expires_at = Carbon::now()->addWeeks(1);
            // }
            $user->remember_token = $token;
            $user->update();
            $userData = json_decode(json_encode([
                '_id' => $user->id,
                'name' => $user->name,

            ]));
            return response()->json([
                // 'id' => $user->id,
                'user' =>  $userData,
                // 'name' => $user->name,
                'token' => $user->remember_token,
                'message' => 'Login successful',
            ], 200);
            // return response()->json(['success'=>true, 'data'=>['id'=>$user->id,'auth_token'=>$user->remember_token,'name'=>$user->name, 'email'=>$user->email]], 200);

        } else {
            return response()->json(['request' => $credentials, 'message' => 'Invalid email or password'], 401);
        }
    }
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return response()->json(['message' => 'Logged Out'], 200);
    }

    public function getUser($user)
    {
        $user= User::findOrFail($user);
        return response($user);
    }

}
