<?php
/* firstIE api
 * From an input text, subtrings that match term are extracted. The goal is to
 * extract the first expression matching a term from right to left.
 * Developer:  Gabriel Carrillo
 * Version  :  2.3.1
 * Updated  :  13 April 2021
 *             23 December 2020    Improved logic for searching longest term from right to left
 */

namespace App\Http\Controllers\Api\Spertabot;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Config;

class SpertabotController extends \App\Http\Controllers\Controller{


    /*------------------------------------------------------------------
     * getBot
     * Return an array with longest expressions and question translation
     *-----------------------------------------------------------------*/

   public function getBot(Request $request) {

    // this woks only with POST

       $orgid       = trim($request->input('orgid' ));
       $userid      = trim($request->input('userid' ));
       $language    = trim($request->input('language' ));
       $inquiry     = trim($request->input('inquiry' ));
       $answer      = array();
       $message     = "";
       $rFormat ="text";

       if ($inquiry ==  "Construction plan") {
          $orgid = 2;
       }

       if ($inquiry ==  "What are your services") {
          $orgid = 5;
       }


        switch ($orgid) {

            case 1:
              $rFormat    = "button";
              $message    = $inquiry." My name is Spertabot. How can I help you? Thank you for reaching out to spertatek Customer Support.

              Please click the button below and an Agent will be with you ASAP. 👍

              We appreciate your patience! ";
              $solText    = "Transfer to a live agent.";
              $solSubject = "Construction Plan";
              $solValue   = "changeAuthStatusBotChat()";
              $solRating  = 30;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              break;

              break;

            case 2:
              $rFormat    = "button";
              $message    = "I found this solution. ";
              $solText    = "I have information on Construction plan.";
              $solSubject = "Construction Plan";
              $solValue   = "***123*45678901234567890";
              $solRating  = 30;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              break;

            case 3:
              $rFormat    = "button";
              $message    = "I found this solution. ";
              $solText    = "I have information on Construction plan.";
              $solSubject = "Construction Plan";
              $solValue   = "***123*45678901234567890";
              $solRating  = 30;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];

              $solText    = "I have information on Building Permit.";
              $solSubject = "Building Permit";
              $solValue   = "***123*45678901234567111";
              $solRating  = 25;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              break;

            case 4:
              $rFormat    = "button";
              $message    = "I found this solution. ";
              $solText    = "I have information on Construction plan.";
              $solSubject = "Construction Plan";
              $solValue   = "***123*45678901234567890";
              $solRating  = 30;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];

              $solText    = "I have information on Building Permit.";
              $solSubject = "Building Permit";
              $solValue   = "***123*45678901234567111";
              $solRating  = 25;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];

              $solText    = "I have information on Remodeling.";
              $solSubject = "Remodeling";
              $solValue   = "***123*45678901234567177";
              $solRating  = 23;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              break;

            case 5:
              $rFormat    = "button";
              $message    = "I found this solution. ";
              $solText    = "I have information on Construction plan.";
              $solSubject = "Construction Plan";
              $solValue   = "***123*45678901234567890";
              $solRating  = 30;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];

              $solText    = "I have information on Building Permit.";
              $solSubject = "Building Permit";
              $solValue   = "***123*45678901234567111";
              $solRating  = 25;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];

              $solText    = "I have information on Remodeling.";
              $solSubject = "Remodeling";
              $solValue   = "***123*45678901234567177";
              $solRating  = 23;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              $solText    = "I have information on Project management.";
              $solSubject = "Project management";
              $solValue   = "***123*45678901234567188";
              $solRating  = 21;
              $answer[] = ['text'=>$solText ,'subject'=>$solSubject , 'value'=>$solValue , 'rating'=>$solRating ];
              break;

        }


	  // return reponse
	  return \Response::json([ 'rFormat'=>$rFormat,'orgid'=>$orgid, 'userid'=>$userid,
      'language' => $language, 'message' => $message,'answer' => $answer ]);


	}



}
