<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message;
use App\Events\NewMessage;

class MessageController extends Controller
{

    public function __construct(){
        $this->middleware('auth:sanctum');
    }

    
    public function getMessages($id)
    {
        $res = Message::where('chatId', '=', $id)->get();
        return $res;
        foreach ($res as $r) {

            $messageData = json_decode($r);
            $rs[] = $messageData;
        }

        return $rs;
    }
    public function addMessage(Request $request)
    {

        $message = new Message;
        $message->senderId = $request->senderId;
        $message->text = $request->text;
        $message->chatId = $request->chatId;
        $message->save();

        return response()->json($message);
    }
}
