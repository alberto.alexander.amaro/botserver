<?php

namespace App\Http\Controllers;

use Facade\FlareClient\View;
use Illuminate\Contracts\View\View as ViewView;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
   public function getServerSide(Request $request)
   {
      if ($request->ajax()) {
         $categories = Category::select('*');
         return Datatables::of($categories)->make(true);
      }
      return view('serverSide');
   }
}
