<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Organization;
use App\Http\Requests\UserRequest;


class UserController extends Controller
{
    private $oUser;
    private $oOrganization;
    public function __construct()
    {
        $this->middleware('can:admin.users.index')->only('index');
        $this->middleware('can:admin.users.edit')->only('edit', 'update');
        $this->oUser = new User;
        $this->oOrganization = new Organization;
    }

    public function index()
    {
        $users = $this->oUser->getUsersById();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = $this->oUser->getUsersRadioById();
        $organizations = $this->oOrganization->getOrganizationSelectById();
        return view('admin.users.create', compact('organizations', 'roles'));
    }


    public function store(UserRequest $request)
    {
        $roles = $this->oUser->getUsersRadioById();
        $role = $roles->find($request->role);
        if ($role) {
            $result = $this->oUser->addUser(
                $request->all()
            );
            $request->session()->put('class', $result['class']);
            $request->session()->put('alert', $result['alert']);
            $request->session()->put('message', $result['message']);
            return redirect()->route('admin.users.index', 'created&new&organization=' . $request->orgName)->with('status', 'Error', 'class', $result['class'], 'alert', $result['message'], 'class', $result['message']);
        }
    }

    public function edit($user)
    {
        $user = $this->oUser->getUsersForEditById($user);
        $roles = $this->oUser->getUsersRadioById();
        $organizations = $this->oOrganization->getOrganizationSelectById();
        return view('admin.users.edit', compact('user', 'roles', 'organizations'));
    }

    public function update(UserRequest $request, $user)
    {
        $roles = $this->oUser->getUsersRadioById();
        $role = $roles->find($request->role);
        if ($role) {
            $result = $this->oUser->updateUser(
                $user,
                $request->all()
            );
            $request->session()->put('class', $result['class']);
            $request->session()->put('alert', $result['alert']);
            $request->session()->put('message', $result['message']);
            return redirect()->route('admin.users.index', 'created&new&organization=' . $request->userName)->with('status', 'Error', 'class', $result['class'], 'alert', $result['message'], 'class', $result['message']);
        }
    }
}
