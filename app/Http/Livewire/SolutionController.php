<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SolutionController extends Component
{
    public function render()
    {
        return view('livewire.solution-controller');
    }
}
