<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionsController extends Component
{
    public $permissionSelected = [], $old_permissions = [], $componentName, $role;
    public function mount()
    {
        $this->componentName = 'Asignar Permisos';
        $this->role = 'Elegir';
    }
    public function render()
    {
        $permissions = Permission::select('name', 'id', 'description', DB::raw("0 as checked"))
            ->orderBy('id', 'asc')
            ->get();
        if ($this->role != 'Elegir') {
            $list = Permission::join('role_has_permissions as rp', 'rp.permission_id', 'permissions.id')
                ->where('role_id', '=', $this->role)->pluck('permissions.id')->toArray();
            $this->old_permissions = $list;
        }
        if ($this->role != 'Elegir') {
            foreach ($permissions as $permission) {
                $role = Role::find($this->role);
                $tienepermiso = $role->hasPermissionTo($permission->name);
                if ($tienepermiso) {
                    $permission->checked = 1;
                }
            }
        }
        $roles = Role::orderBy('name', 'asc')->get();
        $roles = $roles->where('name', '<>', 'Superuser');//Evitar que pase el Superuser
        return view('livewire.permissions-controller', [
            'roles' => $roles,

            'permissions' => $permissions,
        ])->extends('layouts.theme.app')
            ->section('content');
    }
    public $listeners = ['revokeall' => 'RemoveAll'];

    public function RemoveAll()
    {
        if ($this->role == 'Elegir') {
            $this->emit('sync-error', 'Seleccione un role valido');
            return;
        }
        $role = Role::find($this->role);
        $role->syncPermissions([0]);
        $this->emit('removeall', "Se revocaron todos los permisos al role $role->name");
    }
    public function SyncAll()
    {
        if ($this->role == 'Elegir') {
            $this->emit('sync-error', 'Seleccione un role valido');
            return;
        }
        $role = Role::find($this->role);
        $permissions = Permission::pluck('id')->toArray();
        $role->syncPermissions($permissions);
        $this->emit('syncall', "Se sincronizaron todos los permisos al role $role->name");
    }
    public function SyncPermission($state, $permissionName)
    {
        if ($this->role != 'Elegir') {
            $roleName = Role::find($this->role);
            if ($state) {
                $roleName->givePermissionTo($permissionName);
                $this->emit('permi', 'Permiso asignado correctamente');
            } else {
                $roleName->revokePermissionTo($permissionName);
                $this->emit('permi', 'Permiso eliminando correctamente');
            }
        } else {
            $this->emit('sync-error', 'Seleccione un role valido');
        }
    }
}
