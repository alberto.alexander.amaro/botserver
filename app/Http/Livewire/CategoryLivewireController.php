<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Livewire\WithPagination;

class CategoryLivewireController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    private $oCategory;  
    public $name, $categoryName, $category, $categoryid, $search, $image, $selected_id, $pageTitle,
        $componentName, $evented, $viewMode;
    public $updateMode = false;
    public $indexMode = true;
    public $createMode = false;      
    public $sort = 'categoryId';
    public $direction = 'asc';
    public $pagination = 7;

    public function order($sort)
    {
        if ($this->sort == $sort) {

            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function __construct()
    {
        $this->oCategory = new Category;
    }

    public function mount()
    {
        $this->componentName = 'Category';
        $this->pageTitle = 'List';
    }

    public function render()
    {
        // dd(
        //     $this->updateMode.' '. 
        //     $this->selected_id.' '. 
        //     $this->search.' '. 
        //     $this->sort.' '. 
        //     $this->direction.' '. 
        //     $this->pagination
        // );
        // if ($this->updateMode) {
        //     $data = $this->oCategory->findById($this->selected_id);
        //     // dd($data);
        // } else {
        //     if (strlen($this->search) > 0) {
        //         $data = Category::where('categoryName', 'LIKE', '%' . $this->search . '%')
        //             ->orderBy($this->sort, $this->direction)
        //             ->paginate($this->pagination);
        //     } else {
        //         $data = Category::orderBy($this->sort, $this->direction)
        //             ->paginate($this->pagination);
        //     }
        // }
        $data = $this->oCategory->renderLivewire(
            $this->updateMode, 
            $this->selected_id, 
            $this->search, 
            $this->sort, 
            $this->direction, 
            $this->pagination
        );
        if ($this->createMode) {
            $this->viewRender = 'livewire.category.create';
        } elseif ($this->updateMode) {
            $this->viewRender = 'livewire.category.edit';
        } else {
            $this->viewRender = 'livewire.category.index';
        }
        return view('livewire.category-controller', [
            'data' => $data,
            'viewRender' => $this->viewRender,
        ]);
        // return view('livewire.category-controller', [
        //     'data' => $data
        // ])->extends('layouts.theme.app')
        //     ->section('content');
    }

    public function create()
    {
        $this->createMode = true;
        $this->indexMode = false;
        $this->updateMode = false;
        $this->viewMode ='create';
    }
    protected function rules()
    {
        return [
            'categoryName' => "required|min:3|unique:category,categoryName,  {$this->selected_id}  ,categoryId",
        ];
    }
    public function store(Request $request)
    {

        $this->validate();
        $this->oCategory->addCategory(
            $this->createMode,
            $request->all()
        );
        

        $this->indexMode = true;
        $this->createMode = false;
        $this->updateMode = false;
        $this->resetData();
        // $this->emit('row-added', 'Record added succesfully');
        $this->emit('row-added',  [trans('multi-lang.added'), trans('multi-lang.eadded')]);
        $this->evented='added';
        
    }


    public function edit($id)
    {
        $this->selected_id = $id;
        $this->updateMode = true;
        $this->indexMode = false;
        $this->createMode = false;
        $data = $this->oCategory->findById($id);
        foreach ($data as $key) {
            $data = $key;
        }

        $this->categoryName = $data->categoryName;
    }

    public function update(Request $request)
    {
        $this->validate();
        $this->oCategory->updateCategory(
            $this->updateMode,
            $this->selected_id,
            $request->all()
        );

        $this->indexMode = true;
        $this->updateMode = false;
        $this->createMode = false;
        $this->resetData();
        // $this->emit('row-updated', 'Record update succesfully');
        $this->emit('row-updated',  [trans('multi-lang.updated'), trans('multi-lang.eupdated')]);
       

    }

    protected $listeners = [
        'deleteRow' => 'destroy',
    ];

    public function destroy($id)
    {
        $result = $this->oCategory->deleteCategory($id);
        // dd($result);
        // $this->emit('row-deleted', 'Record deleted succesfully');
        $this->emit('row-deleted',  [trans('multi-lang.deleted'), trans('multi-lang.edeleted')]);
    }

    public function ResetData()
    {
        $this->categoryName = '';
        $this->selected_id = 0;
        $this->search = '';
        $this->resetValidation();
    }
}
