<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Component
{
    private $oCategory;
    public $name, $barcode, $cost, $price, $stock, $alerts, $categoryid, $search, $image, $selected_id, $pageTitle,
    $componentName;
    
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('can:category.index')->only('index');
        // $this->middleware('can:category.create')->only('create');
        // $this->middleware('can:category.store')->only('store');
        // $this->middleware('can:category.edit')->only('edit');
        // $this->middleware('can:category.update')->only('update');
        // $this->middleware('can:category.destroy')->only('destroy');       
        $this->oCategory = new Category;
    }
    
    public function mount()
    {
        $this->componentName = 'Category';
        $this->pageTitle = 'List';

    }

    public function render()
    {
        $data = $this->oCategory->getCategoryOrderById();
        return view('livewire.category-controller', [
            'data' => $data
        ])->extends('layouts.theme.app')
            ->section('content');
    }
    
    public function index()
    {
        $data = $this->oCategory->getCategoryOrderById();
        // return view('livewire.category-controller', compact('data'));

        return view('livewire.category-controller', [
            'data' => $data
        ])->extends('layouts.theme.app')
            ->section('content');

        $this->emit('product-updated', 'Producto actualizado');
    }
    public function create()
    {
        return view('livewire.category.create');
    }


    public function store(Request $session, CategoryRequest $request)
    {
        $result = $this->oCategory->addCategory(
            $request->all()
        );
        $session->session()->put('class', $result['class']);
        $session->session()->put('alert', $result['alert']);
        $session->session()->put('message', $result['message']);
        return redirect()->route('category.index', 'created&new&category=' . $request->categoryName)->with('status', 'Error', 'class', $result['class'], 'alert', $result['message'], 'class', $result['message']);
    }


    public function edit($id)
    {
        dd('Saludos');  
        $data = $this->oCategory->findById($id);
        return view('livewire.category.edit', compact('data'));
    }

    public function update(Request $session, CategoryRequest $request, $id)
    {

        $result = $this->oCategory->updateCategory(
            $id,
            $request->all()
        );
        $session->session()->put('class', $result['class']);
        $session->session()->put('alert', $result['alert']);
        $session->session()->put('message', $result['message']);
        return redirect()->route('category.index', 'updated&category=' . $request->categoryName)->with('status', 'Error', 'class', $result['class'], 'alert', $result['message'], 'class', $result['message']);
    }
    protected $listeners = [
        'deleteRow' => 'Destroy',
        'scan-code' => 'ScanCode'
    ];

    // public function destroy(Request $request, $id)
    // {
    //     dd('saludos');
    //     $result = $this->oCategory->deleteCategory($id);
    //     $request->session()->put('class', $result['class']);
    //     $request->session()->put('alert', $result['alert']);
    //     $request->session()->put('message', $result['message']);
    //     // return redirect()->route('category.index', 'deleted&category=' . $id. $result['alert'])->with('status', 'Error', 'class', $result['class'], 'alert', $result['message'], 'class', $result['message']);
       
    // }
    public function Destroy($id)
    {
        dd('hola');
        // $imageName = $product->imagen;
        // $product->delete();
        // if($imageName !== null) {
        //     if ($imageName !== 'noimg.png'){
        //         unlink('storage/products/' . $imageName);
        //     }
        // }
        // if ($imageName != null) {
        //     if (file_exists('storage/products/' . $imageName)) {
        //         if ($imageName !== 'noimg.png') {
        //             unlink('storage/products/' . $imageName);
        //         }
        //     }
        // }
        // $this->resetUI();
        // $this->emit('product-deleted', 'Producto eliminado');
    }
    
}
