<?php

namespace App\View\Components;

use Illuminate\View\Component;
use phpDocumentor\Reflection\Types\This;

class ThShort extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $th;
    public $thName;
    public $sort;
    public $direction;

    public function __construct($th, $thName, $sort, $direction)
    {
        $this->th = $th;
        $this->thName = $thName;
        $this->sort = $sort;
        $this->directionn = $direction;
  
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.th-short');
    }
}
