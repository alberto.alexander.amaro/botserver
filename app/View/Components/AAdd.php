<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AAdd extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $href;
    public $psv;
    public $action;

   public function __construct($href, $psv, $action)
   {
       $this->href = $href;
       $this->psv = $psv;
       $this->action = $action;
 
   }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.a-add');
    }
}
