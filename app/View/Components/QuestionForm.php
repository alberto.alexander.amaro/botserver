<?php

namespace App\View\Components;

use Illuminate\View\Component;

class QuestionForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $organizations;
    public $scope;

   public function __construct($organizations, $scope)
   {
       $this->organizations = $organizations;
       $this->scope = $scope;
   }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.question-form');
    }
}
