<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SolutionForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

     public $language;
     public $filetype;
     public $openin;
     public$organizations;
     public $scope;
    public function __construct($language, $filetype, $openin, $organizations, $scope )
    {
        $this->language = $language;
        $this->filetype = $filetype;
        $this->openin = $openin;
        $this->organizations = $organizations;
        $this->scope = $scope;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.solution-form');
    }
}
