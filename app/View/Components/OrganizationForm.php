<?php

namespace App\View\Components;

use Illuminate\View\Component;

class OrganizationForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $orgLanguage;
    public $isDefault;
    
   public function __construct($orgLanguage, $isDefault)
   {
       $this->orgLanguage = $orgLanguage;
       $this->isDefault = $isDefault;

   }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.organization-form');
    }
}
