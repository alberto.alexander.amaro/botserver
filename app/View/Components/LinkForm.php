<?php

namespace App\View\Components;

use Illuminate\View\Component;

class LinkForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $id;
    public $host;
    public $member;
    public $solution;
    public $linked;
    
   public function __construct($id, $host, $member, $solution, $linked)
   {
       $this->id = $id;
       $this->host = $host;
       $this->member = $member;
       $this->solution = $solution;
       $this->linked = $linked;


   }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.link-form');
    }
}
