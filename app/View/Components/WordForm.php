<?php

namespace App\View\Components;

use Illuminate\View\Component;

class WordForm extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $categories;
    public $organizations;
    public $scope;

   public function __construct($categories, $organizations, $scope)
   {
       $this->categories = $categories;
       $this->organizations = $organizations;
       $this->scope = $scope;
   }



    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.word-form');
    }
}
