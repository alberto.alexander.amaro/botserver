<?php
return [
    'title-application' => 'Spertatek | Un Assistant Virtuel pour les Organisations Modernes',
    'subTitle-application' => 'Un Assistant Virtuel pour les Organisations Modernes',

// NAV

'language' => 'Idioma',
'spanish' => 'Espagnol',
'english' => 'Anglais',
'french' => 'Français',
'welcome' => 'Bienvenido',
'profile' => 'Mi Perfil',
'sign-off' => 'Cerrar Sesion',

// MENU
'home' => 'Accueil',


// ==========================================
// Common
// ==========================================
'actions' => 'Actions',
'back'  => 'Précédent',
'edit' => 'Éditer',
'delete' => 'Supprimer',
'add' => 'Ajouter',
'category' => 'Catégorie',
'word'   => 'Mot',
'question' => 'Question',
'solution' => 'Solution',
'organization' => 'Organisation',
'group' => 'Groupe',
'manager' => 'Gestionnaire',
'access' => 'Acces',
'link' => 'Lien',
'user' => 'Utilisateur',
'permission' => 'Autorisation',
'role' => 'Rôle',
'indicates' => 'Indique requis',
'enter' => 'Entrez le nom du mot"',
'showpages' => 'Mostrant la page',
'ofpage' => 'de',
'results' =>   'Résultats',
'search' => 'Rechercher',
'sign-out' => 'Se désconnecter',
'Public'    => 'Public',
'Private' => 'Privé',
'Group' => 'Groupe',
'scope' => 'Portée',
'name' => 'Nom',
'wname' => 'Mot Nom',
'load-ing' => 'Chargement en cours',




'eadded'  => 'Ajouté',
'eupdated' => 'Mettrée à jour',
'edeleted' => 'Supprimée',
'added' => 'Enregistrement ajouté succès',
'updated' => 'Enregistrement mettrée à jour succès',
'deleted' => 'Enregistrement supprimée succès',
'nodeleted' => 'Cet enregistrement a des enregistrements associés',
'enodeleted' => 'Action annulée',
'cancelled' => 'Votre dossier est en sécurité!',

'qlinks' => 'QLiens',
'wlinks' => 'MLiens',
'lsolutions' => 'LSolutions',
'nqlinks' => 'Aucune question disponible',
'nwlinks' => 'Aucun mot disponible',
'nlsolutions' => 'Aucune solution disponible',
'stext'  => 'Texte court',
'linkeds' => '+Soluncion enlazada',
'questionl' =>    '+Enlance de pregunta',





'm-tittle' => 'Voulez-vous vraiment supprimer cet enregistrement?',
'm-text' => 'Cet enregistrement sera définitivement supprimé!',

'm-confirmButtonText' => 'Oui',
'm-cancelButtonText' =>'Non',
'm-ok' => "D'accord",

'wqlink' => 'Lien Question Mot',

// ==========================================
// Base de Conocimiento - Proyectos
// ==========================================
'kbase' => 'Base de connaissances',
'morganization' => 'Organisation du gestionnaire',
'mgorganization' => "Groupe d'organization",
'maccess' => 'Gestionnaire d’accès',



    // ==========================================
    // Solution
    // ==========================================




    // Solution form  ///

    'si-msg' => "Message d'introduction",
    'ss-text' => 'Texte Court',
    'sl-text' => 'Texte Long',
    's-subject' => 'Matière',
    's-language' => 'Langue',
    's-url' => 'URL',
    's-cost' => 'Cost',
    's-file' => 'Nom de fichier',
    's-type' => 'Type de fichier',
    's-o-chatbot' => 'Ouvert à Chabot',
    's-date1' => 'Rendez-vous 1',
    's-date2' => 'Rendez-vous 2',

    'si-emsg' => "Entrez le message d'introduction",
    'ss-etext' => 'Enter short text',
    'sl-etext' => 'Enter long text',
    's-esubject' => 'Enter subject',
    's-eurl' => 'Enter url',
    's-ecost' => 'Enter cost',
    's-edate1' => 'Select Date 1',
    's-edate2' => 'Select Date 2',


// footer
'footer' => 'Copyright ©',
'footer-right' => 'Tous droits réservés.',
//view


//Chat


//headers
'header-blade-search-placeholder' => 'Buscar...',

//role




];

