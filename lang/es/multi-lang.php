<?php
return [
    'title-application' => 'Spertatek | Asistente Virtual para Organizaciones Modernas',
    'subTitle-application' => 'Asistente Virtual para Organizaciones Modernas',

    // NAV

    'language' => 'Idioma',
    'spanish' => 'Español',
    'english' => 'Ingles',
    'french' => 'Fránces',
    'welcome' => 'Bienvenido',
    'profile' => 'Mi Perfil',
    'sign-off' => 'Cerrar Sesion',

    // MENU
    'home' => 'Inicio',


    // ==========================================
    // Common
    // ==========================================
    'actions' => 'Acciones',
    'back'  => 'Regresar',
    'edit' => 'Editar',
    'delete' => 'Eliminar',
    'add' => 'Agregar',
    'category' => 'Categoría',
    'word'   => 'Palabra',
    'question' => 'Pregunta',
    'solution' => 'Solución',
    'organization' => 'Organización',
    'group' => 'Grupo',
    'manager' => 'Administrar',
    'access' => 'Acceso',
    'link' => 'Enlace',
    'user' => 'Usuario',
    'permission' => 'Permiso',
    'role' => 'Rol',
    'indicates' => 'Campos requeridos',
    'enter' => 'Ingrese',
    'showpages' => 'Mostrando página',
    'ofpage' => 'de',
    'results' =>   'Mostrar',
    'search' => 'Buscar',
    'sign-out' => 'Cerrar Sesion',
    'Public'    => 'Publico',
    'Private' => 'Privado',
    'Group' => 'Grupo',
    'scope' => 'Alcance',
    'name' => 'Nombre',
    'wname' => 'Nombre de Palabra',
    'eadded' => 'Agregado',
    'eupdated' => 'Actualizado',
    'edeleted' => 'Eliminado',
    'added' => 'Registo agregado satisfactoriamente',
    'updated' => 'Registo actualizado satisfactoriamente',
    'deleted' => 'Registo eliminado satisfactoriamente',
    'nodeleted' => 'Este regístro tiene asociado otros regístros',
    'enodeleted' => 'Acción Cancelada',
    'cancelled' => 'Su registro está a salvo!',
    'load-ing' => 'Procesando',




    'm-tittle' => '¿Realmente quieres eliminar este registro?',
    "m-text" => "Este registro se eliminará permanentemente!",

    'm-confirmButtonText' => 'Sí',
    'm-cancelButtonText' => 'No',

    'wqlink' => 'Enlace de Pregunta de Palabra',

    'qlinks' => 'PEnlazadas',
    'wlinks' => 'PEnlzadas',
    'lsolutions' => 'ESoluciones',
    'nqlinks' => 'No hay preguntas disponibles',
    'nwlinks' => 'No hay palabras disponibles',
    'nlsolutions' => 'No hay soluciones disponibles',
    'stext'  => 'Texto Corto',
    'linkeds' => '+Solución enlazada',
    'questionl' =>    '+Enlance de pregunta',


    // ==========================================
    // Base de Conocimiento - Proyectos
    // ==========================================
    'kbase' => 'Base de conocimiento',
    'morganization' => 'Administrar Organización',
    'mgorganization' => 'Grupo de Organización',
    'maccess' => 'Administrar Acceso',





    // ==========================================
    // Solution
    // ==========================================




    // Solution form  ///

    'si-msg' => 'Mensaje de introducción',
    'ss-text' => 'Texto Corto',
    'sl-text' => 'Texto Largo',
    's-subject' => 'Asunto',
    's-language' => 'Idioma',
    's-url' => 'URL',
    's-cost' => 'Costo',
    's-file' => 'Nombre del archivo',
    's-type' => 'Tipo de archivo',
    's-o-chatbot' => 'Abrir en Chabot',
    's-date1' => 'Fecha 1',
    's-date2' => 'Fecha 2',

    'si-emsg' => 'Introducir mensaje de introducción',
    'ss-etext' => 'Enter short text',
    'sl-etext' => 'Enter long text',
    's-esubject' => 'Enter subject',
    's-eurl' => 'Enter url',
    's-ecost' => 'Enter cost',
    's-edate1' => 'Select Date 1',
    's-edate2' => 'Select Date 2',



    // footer
    'footer' => 'Copyright ©',
    'footer-right' => 'Todos los derechos reservados.',
    //view


    //Chat


    //headers
    'header-blade-search-placeholder' => 'Buscar...',

    //role




];
