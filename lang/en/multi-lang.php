<?php
return [
    'title-application' => 'Spertatek | A Virtual Assistant for Modern Organizations',
    'subTitle-application' => 'A Virtual Assistant for Modern Organizations',


    // NAV

    'spanish'  => 'Spanish',
    'english'  => 'English',
    'french' => 'French',
    'welcome'  => 'Welcome',
    'profile'  => 'My Profile',
    'sign-off' => 'Sign off',

    // MENU
    'home' => 'Home',


    // ==========================================
    // Common
    // ==========================================
    'actions' => 'Actions',
    'back'  => 'Back',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'add' => 'Add',
    'category' => 'Category',
    'word'   => 'Word',
    'question' => 'Question',
    'solution' => 'Solution',
    'organization' => 'Organization',
    'group' => 'Group',
    'manager' => 'Manager',
    'access' => 'Access',
    'link' => 'Link',
    'user' => 'User',
    'chat' => 'Chat',
    'permission' => 'Permission',
    'role' => 'Role',
    'indicates' => 'Indicates required',
    'enter' => 'Enter',
    'showpages' => 'Showing page',
    'ofpage' => 'of',
    'results' =>   'Results',
    'search' => 'Search',
    'sign-out' => 'Sign Out',
    'Public'    => 'Public',
    'Private' => 'Private',
    'Group' => 'Group',
    'scope' => 'Scope',
    'name' => 'Name',
    'wname' => 'Word Name',
    'eadded'  => 'Added',
    'eupdated' => 'Updated',
    'edeleted' => 'Deleted',
    'added' => 'Record added succesfully',
    'updated' => 'Record updated succesfully',
    'deleted' => 'Record deleted Successfully',
    'nodeleted' => 'This record has associated records',
    'enodeleted' => 'Action Cancelled',
    'cancelled' => 'Your record is safe',
    'wqlink' => 'Word Question Link',
    'load-ing' => 'Proccessing',

    'qlinks' => 'QLinks',
    'wlinks' => 'WLinks',
    'nqlinks' => 'No questions available',
    'nwlinks' => 'No words available',
    'nlsolutions' => 'No solutions available',
    'lsolutions' => 'LSolutions',
    'stext'  => 'Short Text',
    'linkeds' => '+Linked solution',
    'questionl' =>    '+Question link',

    'm-tittle' => 'Do you really want to delete this record?',
    'm-text' => "This record will be permanently deleted!",

    'm-confirmButtonText' => 'Yes',
    'm-cancelButtonText' => 'No',

    // ==========================================
    // Knowledge Base
    // ==========================================
    'kbase' => 'Knowledge base',
    'morganization' => 'Organization Manager',
    'mgorganization' => 'Organization Group',
    'maccess' => 'Access Manager',
    'mchat' => 'Chat',

    // ==========================================
    // Solution
    // ==========================================




    // Solution form  ///

    'si-msg' => 'Intro Message',
    'si-emsg' => 'Enter intro message',
    'ss-text' => 'Short Text',
    'ss-etext' => 'Enter short text',
    'sl-text' => 'Long Text',
    'sl-etext' => 'Enter long text',
    's-subject' => 'Subject',
    's-esubject' => 'Enter subject',
    's-language' => 'Language',
    's-url' => 'URL',
    's-eurl' => 'Enter url',
    's-cost' => 'Cost',
    's-ecost' => 'Enter cost',
    's-file' => 'Filename',
    's-type' => 'Filetype',
    's-o-chatbot' => 'Open in Chabot',
    's-date1' => 'Date 1',
    's-edate1' => 'Select Date 1',
    's-date2' => 'Date 2',
    's-edate2' => 'Select Date 2',




    // footer
    'footer' => 'Copyright ©',
    'footer-right' => 'All rights reserved.',

    //view


    //Chat


    //headers
    'header-blade-search-placeholder' => 'Search...',

];
