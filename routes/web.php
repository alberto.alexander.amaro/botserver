<?php


use App\Http\Controllers\OrganizationGroupController;
use App\Http\Controllers\OrganizationGroupSuperController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\WordController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\WordQuestionController;
use App\Http\Controllers\SolutionLinkController;
use App\Http\Controllers\SolutionController;
use App\Http\Controllers\QuestionSolutionController;
use App\Http\Livewire\PermissionsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Livewire\CategoryLivewireController;
use App\Http\Controllers\Admin\SupplierController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
use PHPUnit\Runner\Hook;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/set_language/{lang}', [App\Http\Controllers\Controller::class, 'set_language'])->name('set_language');

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified'
// ])->group(function () {
//     Route::get('/dashboard', function () {
//         return view('dashboard');
//     })->name('dashboard');
// });


Route::get('message', function () {
    $message['user'] = "Juan Perez";
    $message['message'] =  "Prueba mensaje desde Pusher";
    $success = event(new App\Events\NewMessage($message));
    echo 'Enviado';
});
Route::get('bot', function() {
    return view('message');
  });

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('server-side', [HomeController::class, 'getServerSide'])->name('server.side.index');
Route::get('/lang/{language}', function ($language) {
    Session::put('language', $language);
    return redirect()->back();
})->name('language');
Route::group(['middleware' => config('jetstream.middleware', ['web'])], function () {
    Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
        return view('home');
    })->name('dashboard');
    Route::middleware(['auth:sanctum', 'verified'])->group(function () {
        Route::resource('category', CategoryController::class)->except('show')->names('category')->middleware(['verified']);
        Route::resource('/category/livewire', CategoryLivewireController::class)->except('index')->names('category.livewire')->middleware(['verified']);
        Route::get('/paginateCategory', function () {
            return App\Models\Category::paginate(5);
        });
        Route::resource('word', WordController::class)->except('show')->names('word');

        Route::resource('organization', OrganizationController::class)->except('show')->names('organization');
        Route::resource('organization/group', OrganizationGroupSuperController::class)->except('create')->names('organization.group');
        Route::get('organization/group/{id}/create', [OrganizationGroupSuperController::class, 'organizationGroupCreate'])->name('organization.group.create');

        Route::resource('question', QuestionController::class)->names('question');
        Route::get('question/{id}/link', [WordQuestionController::class, 'questionLinkIndex'])->name('question.link.index');
        Route::get('question/link/{id}/create', [WordQuestionController::class, 'questionLinkCreate'])->name('question.link.create');
        Route::post('question/link/store', [WordQuestionController::class, 'storelink'])->name('question.link.store');
        Route::delete('question/link/{id}/{wq}/destroy', [WordQuestionController::class, 'destroyLink'])->name('question.link.destroy');

        Route::resource('solution', SolutionController::class)->except('show')->names('solution');
        Route::resource('solution/link', SolutionLinkController::class)->except('create')->names('solution.link');
        Route::get('solution/link/{id}/create', [SolutionLinkController::class, 'solutionLinkCreate'])->name('solution.link.create');

        Route::get('solution/{id}/question', [QuestionSolutionController::class, 'questionLinkIndex'])->name('solution.question.index');
        Route::get('solution/question/{id}/create', [QuestionSolutionController::class, 'questionLinkCreate'])->name('solution.question.create');
        Route::post('solution/question/store', [QuestionSolutionController::class, 'storelink'])->name('solution.question.store');
        Route::delete('solution/question/{id}/{wq}/destroy', [QuestionSolutionController::class, 'destroyLink'])->name('solution.question.destroy');

        Route::get('/permissions', PermissionsController::class)->name('assign');
        Route::resource('admin/users', UserController::class)->only(['index', 'create', 'store', 'edit', 'update'])->names('admin.users'); //->middleware(['password.confirm']);
        Route::resource('admin/roles', RoleController::class)->names('admin.roles'); //->middleware(['password.confirm']);
        Route::resource('admin/permissions', PermissionController::class)->names('admin.permissions'); //->middleware(['password.confirm']);
        Route::resource('category/livewire', SupplierController::class)->names('admin.suppliers');
        Route::get('chat', 'App\Http\Controllers\ChatController@index')->name('chat.index');

        Route::get('auth/user', function () {

            if(auth()->check()){

                return response()->json([
                    'authUser' => auth()->user()
                ]);

                return null;

            }

        });
        // Route::middleware('auth:sanctum')->get('/chat/{id}', [App\Http\Controllers\ChatController::class, 'userChats'])->name('chat');

        // Route::get('chat/{chat}', 'App\Http\Controllers\ChatController@show')->name('chat.show');

        Route::get('chat/with/{user}', 'App\Http\Controllers\ChatController@chat_with')->name('chat.with');

        Route::get('chat/{chat}/get_users', 'App\Http\Controllers\ChatController@get_users')->name('chat.get_users');

        Route::get('chat/{chat}/get_messages', 'App\Http\Controllers\ChatController@get_messages')->name('chat.get_messages');

        Route::post('message/sent', '\App\Http\Controllers\MessageController@sent')->name('message.sent');
    });
});
