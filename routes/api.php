<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\Spertabot\SpertabotController;
use App\Http\Controllers\Api\UserApiSpertaController;
use App\Http\Controllers\Api\ChatController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => '/spertabot'], function () {
        Route::post('/{orgid}/{userid}/{language}/{inquiry}', [SpertabotController::class, 'getBot']);
        Route::post('/', [SpertabotController::class, 'getBot']);
    });
});
Route::middleware('auth:sanctum')->get('/chat/{id}', [ChatController::class, 'userChats'])->name('chat');
Route::controller(MessageController::class)->group(function () {
    Route::middleware('auth:sanctum')->get('/messages', 'index');
    Route::middleware('auth:sanctum')->get('/message/{id}', 'getMessages');
    Route::post('/message', 'addMessage');
    Route::middleware('auth:sanctum')->post('/messages', 'store');
});

Route::post('/login', [UserApiSpertaController::class, 'login']);
Route::post('/register', [UserApiSpertaController::class, 'register']);
Route::middleware('auth:sanctum')->get('/user/{id}', [UserApiSpertaController::class, 'getUser']);
// Route::get('/getContacts', [ContactController::class, 'index']);
Route::middleware('auth:sanctum')->post('/logout', [UserApiSpertaController::class, 'logout']);
