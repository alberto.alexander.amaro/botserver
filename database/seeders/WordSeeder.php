<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Word;

class WordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Word::create(['wordName' => 'service', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'services', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what do you do', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what can you do', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what can you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what are your services', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what service can you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what services can you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what service do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'what services do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'which service do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'which services do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'construction permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'do you provide building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        // Word::create(['wordName' => 'can you provide building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);

        // Word::create(['wordName' => 'I need a quotation', 'categoryId' => 	'2', 'scope' => '0', 'orgId' => '1' ]);
        // Word::create(['wordName' => 'I need a quote', 'categoryId' => 	'2', 'scope' => '0', 'orgId' => '1' ]);
        // Word::create(['wordName' => 'course', 'categoryId' => 	'2', 'scope' => '0', 'orgId' => '1' ]);
        // Word::create(['wordName' => 'course', 'categoryId' => 	'1', 'scope' => '1', 'orgId' => '2' ]);
        // Word::factory(800)->create();

        Word::create(['wordName' => 'What are your services', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'What do you do', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'What can you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'What services do you provide', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Do you provide building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Can you provide building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Can you get a building permit', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Do you provide construction plans', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Can you provide a construction plan', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Can you get me a construction plan', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '2' ]);
        Word::create(['wordName' => 'Software development', 'categoryId' => 	'2', 'scope' => '0', 'orgId' => '1' ]);
        Word::create(['wordName' => 'What services they offer', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '3' ]);
        Word::create(['wordName' => 'What kind of technical support do you offer', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '3' ]);
        Word::create(['wordName' => 'What is the employee learning curve', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '3' ]);
        Word::create(['wordName' => 'What technology do they use', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '3' ]);
        Word::create(['wordName' => 'What methodology do you use to manage the projects', 'categoryId' => 	'2', 'scope' => '1', 'orgId' => '3' ]);

    }

}
