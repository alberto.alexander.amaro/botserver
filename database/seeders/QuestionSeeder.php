<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Question::create(['questionName' => 'services', 'scope' => '1', 'orgId' => '2' ]);
        // Question::create(['questionName' => 'building permit', 'scope' => '1', 'orgId' => '2' ]);
        // Question::create(['questionName' => 'quotation', 'scope' => '0', 'orgId' => '1' ]);
        Question::create(['questionName' => 'services', 'scope' => '1', 'orgId' => '2' ]);
        Question::create(['questionName' => 'building permit', 'scope' => '1', 'orgId' => '2' ]);
        Question::create(['questionName' => 'Construction Plan', 'scope' => '1', 'orgId' => '2' ]);
        Question::create(['questionName' => 'Software development', 'scope' => '0', 'orgId' => '1' ]);
        Question::create(['questionName' => 'services offer', 'scope' => '1', 'orgId' => '3' ]);
        Question::create(['questionName' => 'technical support', 'scope' => '1', 'orgId' => '3' ]);
        Question::create(['questionName' => 'learning curve', 'scope' => '1', 'orgId' => '3' ]);
        Question::create(['questionName' => 'technology do they use', 'scope' => '1', 'orgId' => '3' ]);
        Question::create(['questionName' => 'methodology do you use', 'scope' => '1', 'orgId' => '3' ]);


        // Question::factory(500)->create();

    }


}
