<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Solution;

class SolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Solution::create([
        //     'longText' => 'We can provide building permit',
        //     'shortText' => 'We can provide building permit',
        //     'subject' => 'building permit',
        //     'language' => 'en',
        //     'scope' => '1',
        //     'orgId' => '2'
        // ]);
        // Solution::create([
        //     'longText' => 'We can provide construction permit',
        //     'shortText' => 'We can provide construction permit',
        //     'subject' => 'construction permit',
        //     'language' => 'en',
        //     'scope' => '1',
        //     'orgId' => '2'
        // ]);
        // Solution::create([
        //     'longText' => 'We can remodel your home',
        //     'shortText' => 'We can remodel your home',
        //     'subject' => 'remodelling',
        //     'language' => 'en',
        //     'scope' => '1',
        //     'orgId' => '2'
        // ]);
        // Solution::create([
        //     'longText' => 'We can build your home',
        //     'shortText' => 'We can build your home',
        //     'subject' => 'home building',
        //     'language' => 'en',
        //     'scope' => '1',
        //     'orgId' => '2'
        // ]);


        Solution::create([
            'longText' => 'We can prepare building permit',
            'shortText' => 'We can prepare building permit',
            'subject' => 'building permit',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '2'
        ]);
        Solution::create([
            'longText' => 'We can prepare construction plan',
            'shortText' => 'We can prepare construction plan',
            'subject' => 'construction permit',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '2'
        ]);
        Solution::create([
            'longText' => 'We can remodel houses',
            'shortText' => 'We can remodel houses',
            'subject' => 'remodelling',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '2'
        ]);
        Solution::create([
            'longText' => 'We can build houses',
            'shortText' => 'We can build houses',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '2'
        ]);


        Solution::create([
            'longText' => 'We can render plans',
            'shortText' => 'We can render plans',
            'subject' => 'building permit',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '2'
        ]);
        Solution::create([
            'longText' => 'We can develop software',
            'shortText' => 'We can develop software',
            'subject' => 'construction permit',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);
        Solution::create([
            'longText' => 'We can develop Chatbot',
            'shortText' => 'We can develop Chatbot',
            'subject' => 'remodelling',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);
        Solution::create([
            'longText' => 'We can develop chat software',
            'shortText' => 'We can develop chat software',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);
        Solution::create([
            'longText' => 'We can integrate AWS Lex bot',
            'shortText' => 'We can integrate AWS Lex bot',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);

        Solution::create([
            'longText' => 'We can develop frame-based chabot',
            'shortText' => 'We can develop frame-based chabot',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);
        Solution::create([
            'longText' => 'We can develop RPA software',
            'shortText' => 'We can develop RPA software',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '0',
            'orgId' => '1'
        ]);



        Solution::create([
            'longText' => 'We develop projects with web technology',
            'shortText' => 'We develop projects with web technology',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);



        Solution::create([
            'longText' => 'We provide technical support to customers',
            'shortText' => 'We provide technical support to customers',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);


        Solution::create([
            'longText' => 'We provide advice on the use of technologies to potential clients',
            'shortText' => 'We provide advice on the use of technologies to potential clients',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);
        Solution::create([
            'longText' => 'In all our range of products and services offered, technical support is guaranteed',
            'shortText' => 'Or products and services offered, technical support is guaranteed',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);
        Solution::create([
            'longText' => 'Our developments focus on the simplicity of processes, which considerably reduces interaction with the system, maximizing productivity and reducing learning time.',
            'shortText' => 'Our developments focus on the simplicity of processes',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);
        Solution::create([
            'longText' => 'We can actually program in the tool that the client wants, however, we specialize in Laravel, Livewire, React, Websockets and Mysql. Not only because we like its simplicity and "cleanliness", but because of the speed with which it allows solutions to be implemented. The faster our client can see progress, the better it is for us',
            'shortText' => 'Laravel, Livewire, React, Websockets and Mysql',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);

        Solution::create([
            'longText' => 'We use a process adapted from Scrum and Lean Startup, where we talk with our clients about their requirements, we make a prioritized list. The top of the list are occupied by the things that will bring the most value to the client, which are usually the ones that worry him the most or "hurt".',
            'shortText' => 'Scrum and Lean Startup',
            'subject' => 'home building',
            'language' => 'en',
            'scope' => '1',
            'orgId' => '3'
        ]);



















        // scope 0 orgId 1
        // Solution::create([
        //     'longText' => 'We can build your software',
        //     'shortText' => 'A We can build your software',
        //     'subject' => 'software building',
        //     'language' => 'en',
        //     'scope' => '0',
        //     'orgId' => '1'
        // ]);
        // Solution::create([
        //     'longText' => 'We can provide arquictecture information permit',
        //     'shortText' => 'B We can provide arquictecture information permit',
        //     'subject' => 'arquictecture permit',
        //     'language' => 'en',
        //     'scope' => '0',
        //     'orgId' => '1'
        // ]);
        // Solution::create([
        //     'longText' => 'We can provide information permit',
        //     'shortText' => 'C We can provide information permit',
        //     'subject' => 'information permit',
        //     'language' => 'en',
        //     'scope' => '0',
        //     'orgId' => '1'
        // ]);
        // Solution::create([
        //     'longText' => 'We can remodel your software',
        //     'shortText' => 'D We can remodel your software',
        //     'subject' => 'remodelling',
        //     'language' => 'en',
        //     'scope' => '0',
        //     'orgId' => '1'
        // ]);

        // scope 1 orgId 1
    //     Solution::create([
    //         'longText' => 'We can build your software',
    //         'shortText' => 'E We can build your software',
    //         'subject' => 'software building',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide arquictecture information permit',
    //         'shortText' => 'F We can provide arquictecture information permit',
    //         'subject' => 'arquictecture permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide information permit',
    //         'shortText' => 'G We can provide information permit',
    //         'subject' => 'information permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your software',
    //         'shortText' => 'H We can remodel your software',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '1'
    //     ]);
    //     // scope 2 orgId 1
    //     Solution::create([
    //         'longText' => 'We can build your software',
    //         'shortText' => 'I We can build your software',
    //         'subject' => 'software building',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide arquictecture information permit',
    //         'shortText' => 'J We can provide arquictecture information permit',
    //         'subject' => 'arquictecture permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide information permit',
    //         'shortText' => 'K We can provide information permit',
    //         'subject' => 'information permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '1'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your software',
    //         'shortText' => 'M We can remodel your software',
    //         'subject' => 'remodelling software',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '1'
    //     ]);


    //     // scope 1 orgId 2
    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'N We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'O We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'P We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'Q We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '2'
    //     ]);


    //     // scope 2 orgId 2
    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'R We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'S We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'T We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '2'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'U We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '2'
    //     ]);


    //     // scope 1 orgId 3
    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'W We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'X We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'Y We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'Z We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '3'
    //     ]);

    //     // scope 2 orgId 3

    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'A We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'B We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'C We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '3'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'D We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '3'
    //     ]);

    //     // scope 1 orgId 4
    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'E We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'F We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'G We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'J We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '1',
    //         'orgId' => '4'
    //     ]);

    //     // scope 2 orgId 4
    //     Solution::create([
    //         'longText' => 'We can provide building permit',
    //         'shortText' => 'K We can provide building permit',
    //         'subject' => 'building permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can provide construction permit',
    //         'shortText' => 'L We can provide construction permit',
    //         'subject' => 'construction permit',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can remodel your home',
    //         'shortText' => 'M We can remodel your home',
    //         'subject' => 'remodelling',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '4'
    //     ]);
    //     Solution::create([
    //         'longText' => 'We can build your home',
    //         'shortText' => 'N We can build your home',
    //         'subject' => 'home building',
    //         'language' => 'en',
    //         'scope' => '2',
    //         'orgId' => '4'
    //     ]);
     }
}
