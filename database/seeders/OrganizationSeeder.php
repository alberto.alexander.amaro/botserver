<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Organization;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::create([ 
             'orgName' => 	'SPERTATEK',
             'orgContact' => 	'Gabriel Carrillo', 
             'orgEmail' => 	'gabrielcarrillo@spertatek.com',
             'orgLanguage' => 	'en',
             'orgIsDefault' => 	'1',
             'orgIsActive' => 	'1']
            );
        Organization::create([ 
                'orgName' => 	'JD RIVERO',
                'orgContact' => 	'José Domingo Rivero', 
                'orgEmail' => 	'josedomingorivero@jdrivero.com',
                'orgLanguage' => 	'en',
                'orgIsDefault' => 	'0',
                'orgIsActive' => 	'1']
               );
        Organization::create([ 
                'orgName' => 	'CLICKSNOWSOLUTIONS',
                'orgContact' => 	'Alberto Amaro', 
                'orgEmail' => 	'albertoamaro@gmail.com',
                'orgLanguage' => 	'en',
                'orgIsDefault' => 	'0',
                'orgIsActive' => 	'1']
               );
               Organization::create([ 
                'orgName' => 	'RIVERO VISUAL GROUP',
                'orgContact' => 	'Vioscar Rivero', 
                'orgEmail' => 	'vioscarrivero@riverovisualgroup.com',
                'orgLanguage' => 	'en',
                'orgIsDefault' => 	'0',
                'orgIsActive' => 	'1']
               );
    }
}
