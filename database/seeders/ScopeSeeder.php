<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Scope;

class ScopeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Scope::create([  'scopeId' => 0, 'scopeName' => 	'Public' ]);
        Scope::create([  'scopeId' => 1, 'scopeName' => 	'Private' ]);        
        Scope::create([  'scopeId' => 2, 'scopeName' => 	'Group' ]);

    }
    
}
