<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrganizationGroup;

class OrganizationGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        OrganizationGroup::create(['hostOrganizationGroupId' => '1', 'organizationId' => '2' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '1', 'organizationId' => '3' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '1', 'organizationId' => '4' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '2', 'organizationId' => '1' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '2', 'organizationId' => '3' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '2', 'organizationId' => '4' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '3', 'organizationId' => '1' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '3', 'organizationId' => '2' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '3', 'organizationId' => '4' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '4', 'organizationId' => '1' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '4', 'organizationId' => '2' ]);
        OrganizationGroup::create(['hostOrganizationGroupId' => '4', 'organizationId' => '3' ]);


    }
    
}
