<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TmpSolution;

class TmpSolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        TmpSolution::create([
            'solutionId' => '1',
            'longName' => 'We can provide building permit', 
            'shortName' => 'We can provide building permit', 
            'subject' => 'building permit', 
            'language' => 'en', 
            'scope' => '1', 
            'orgId' => '2' 
        ]);
        TmpSolution::create([
            'solutionId' => '2',
            'longName' => 'We can provide construction permit', 
            'shortName' => 'We can provide construction permit', 
            'subject' => 'construction permit', 
            'language' => 'en', 
            'scope' => '1', 
            'orgId' => '2' 
        ]);
        TmpSolution::create([
            'solutionId' => '3',
            'longName' => 'We can remodel your home', 
            'shortName' => 'We can remodel your home', 
            'subject' => 'remodelling', 
            'language' => 'en', 
            'scope' => '1', 
            'orgId' => '2' 
        ]);
        TmpSolution::create([
            'solutionId' => '4',
            'longName' => 'We can build your home', 
            'shortName' => 'We can build your home', 
            'subject' => 'home building', 
            'language' => 'en', 
            'scope' => '1', 
            'orgId' => '2' 
        ]);       

    }
    
}
