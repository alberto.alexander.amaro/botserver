<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Chat;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Chat::create(['members' => [1, 2]]);
        Chat::create(['members' => [1, 3]]);
        Chat::create(['members' => [1, 4]]);
        Chat::create(['members' => [1, 5]]);
        Chat::create(['members' => [2, 3]]);
        Chat::create(['members' => [2, 5]]);
        Chat::create(['members' => [2, 4]]);
        Chat::create(['members' => [3, 4]]);
        Chat::create(['members' => [3, 5]]);
        Chat::create(['members' => [4, 5]]);
    }
}
