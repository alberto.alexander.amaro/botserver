<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WordQuestion;

class WordQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // WordQuestion::create(['wordId' => '1', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '2', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '3', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '4', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '5', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '6', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '7', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '8', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '9', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '10', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '11', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '12', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '13', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '14', 'questionId' => '1' ]);
        // WordQuestion::create(['wordId' => '15', 'questionId' => '2' ]);
        // WordQuestion::create(['wordId' => '16', 'questionId' => '2' ]);
        // WordQuestion::create(['wordId' => '17', 'questionId' => '2' ]);
        // WordQuestion::create(['wordId' => '18', 'questionId' => '2' ]);
        // WordQuestion::create(['wordId' => '19', 'questionId' => '3' ]);
        // WordQuestion::create(['wordId' => '20', 'questionId' => '3' ]);
        // WordQuestion::create(['wordId' => '21', 'questionId' => '3' ]);

        WordQuestion::create(['wordId' => '1', 'questionId' => '1' ]);
        WordQuestion::create(['wordId' => '2', 'questionId' => '1' ]);
        WordQuestion::create(['wordId' => '3', 'questionId' => '1' ]);
        WordQuestion::create(['wordId' => '4', 'questionId' => '1' ]);
        WordQuestion::create(['wordId' => '5', 'questionId' => '2' ]);
        WordQuestion::create(['wordId' => '6', 'questionId' => '2' ]);
        WordQuestion::create(['wordId' => '7', 'questionId' => '2' ]);
        WordQuestion::create(['wordId' => '8', 'questionId' => '3' ]);
        WordQuestion::create(['wordId' => '9', 'questionId' => '3' ]);
        WordQuestion::create(['wordId' => '10', 'questionId' => '3' ]);
        WordQuestion::create(['wordId' => '11', 'questionId' => '4' ]);

        WordQuestion::create(['wordId' => '12', 'questionId' => '5' ]);
        WordQuestion::create(['wordId' => '13', 'questionId' => '6' ]);
        WordQuestion::create(['wordId' => '14', 'questionId' => '7' ]);
        WordQuestion::create(['wordId' => '15', 'questionId' => '8' ]);
        WordQuestion::create(['wordId' => '16', 'questionId' => '9' ]);

    }

}
