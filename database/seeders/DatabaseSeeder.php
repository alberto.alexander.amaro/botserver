<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;





class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ScopeSeeder::class);
        $this->call(OrganizationSeeder::class);
        $this->call(WordSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(SolutionSeeder::class);
        $this->call(TmpSolutionSeeder::class);
        $this->call(WordQuestionSeeder::class);
        // $this->call(QuestionSolutionSeeder::class);
        $this->call(OrganizationGroupSeeder::class);
        $this->call(SolutionLinkSeeder::class);
        $this->call(ChatSeeder::class);
    }
}
