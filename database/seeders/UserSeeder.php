<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Alberto Alexander Amaro',
            'email' => 'alberto.alexander.amaro@clicknowsolutions.com',
            'password' => bcrypt('12345678'),
            'orgId' => '3',
            'isActive' => '1'
        ])->assignRole('Specialuser');

        User::create([
            'name' => 'Rosa Cuevas',
            'email' => 'rosacuevas@clicknowsolutions.com',
            'password' => bcrypt('12345678'),
            'orgId' => '3',
            'isActive' => '1'
        ])->assignRole('Manager');

        User::create([
            'name' => 'Pedro Fuentes',
            'email' => 'pedrofuentes@clicknowsolutions.com',
            'password' => bcrypt('12345678'),
            'orgId' => '3',
            'isActive' => '1'
        ])->assignRole('Staff');

        User::create([
            'name' => 'Henry Hidalgo',
            'email' => 'henryhidalgo@clicknowsolutions.com',
            'password' => bcrypt('12345678'),
            'orgId' => '3',
            'isActive' => '1'
        ])->assignRole('Staff');


        User::create([
            'name' => 'Gabriel Carrillo',
            'email' => 'gabrielcarrillo@spertatek.com',
            'password' => bcrypt('gabriel12345'),
            'orgId' => '1',
            'isActive' => '1',
        ])->assignRole('Superuser');

        User::create([
            'name' => 'Soila Carrillo',
            'email' => 'soilacarrillo@spertatek.com',
            'password' => bcrypt('gabriel12345'),
            'orgId' => '1',
            'isActive' => '1',
        ])->assignRole('Manager');

        User::create([
            'name' => 'Marilyn Blanco',
            'email' => 'marilynblanco@spertatek.com',
            'password' => bcrypt('gabriel12345'),
            'orgId' => '1',
            'isActive' => '1',
        ])->assignRole('Staff');

        User::create([
            'name' => 'Rosmary Bolivar',
            'email' => 'rosmarybolivar@spertatek.com',
            'password' => bcrypt('gabriel12345'),
            'orgId' => '1',
            'isActive' => '1',
        ])->assignRole('Staff');

        User::create([
            'name' => 'Vioscar Rivero',
            'email' => 'vioscarrivero@riverovisualgroup.com',
            'password' => bcrypt('12345678'),
            'orgId' => '4',
            'isActive' => '1',
        ])->assignRole('Manager');

        User::create([
            'name' => 'Pablo Garcia',
            'email' => 'pablogarcia@riverovisualgroup.com',
            'password' => bcrypt('12345678'),
            'orgId' => '4',
            'isActive' => '1',
        ])->assignRole('Staff');

        User::create([
            'name' => 'Maria Rivero',
            'email' => 'mariarivero@riverovisualgroup.com',
            'password' => bcrypt('12345678'),
            'orgId' => '4',
            'isActive' => '1',
        ])->assignRole('Staff');
    
    }
}
