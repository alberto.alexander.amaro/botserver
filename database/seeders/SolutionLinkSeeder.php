<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SolutionLink;

class SolutionLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '13' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '14' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '15' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '16' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '13' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '14' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '15' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '16' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '1' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '2' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '3' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '4' ]);

        SolutionLink::create(['solutionParentId' => '6', 'solutionChildId' => '7' ]);
        SolutionLink::create(['solutionParentId' => '6', 'solutionChildId' => '8' ]);
        SolutionLink::create(['solutionParentId' => '6', 'solutionChildId' => '9' ]);
        SolutionLink::create(['solutionParentId' => '7', 'solutionChildId' => '10' ]);
        SolutionLink::create(['solutionParentId' => '7', 'solutionChildId' => '11' ]);
        SolutionLink::create(['solutionParentId' => '13', 'solutionChildId' => '11' ]);

    }

}
