<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role1 = Role::create(['name' => 'Superuser']);
        $role2 = Role::create(['name' => 'Specialuser']);
        $role3 = Role::create(['name' => 'Manager']);
        $role4 = Role::create(['name' => 'Staff']);

        Permission::create(['name' => 'knowledge.base', 'description' => 'Show Knowledge Base'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.manager', 'description' => 'Show Organization Manager'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'access.manager', 'description' => 'Show Access Manager'])->syncRoles($role1, $role2, $role3, $role4);



        // Permission::create(['name' => 'admin.users.update', 'description' => ''])->syncRoles($role1);
        Permission::create(['name' => 'word.index', 'description' => 'Show word list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'word.create', 'description' => 'Add word'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'word.store', 'description' => 'Store word'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'word.edit', 'description' => 'Edit word'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'word.update', 'description' => 'Update word'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'word.destroy', 'description' => 'Delete word'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'category.index', 'description' => 'Show category list'])->syncRoles($role1, $role2,$role3);
        Permission::create(['name' => 'category.create', 'description' => 'Create category'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'category.store', 'description' => 'Store category'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'category.edit', 'description' => 'Edit category'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'category.update', 'description' => 'Update category'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'category.destroy', 'description' => 'Delete category'])->syncRoles($role1, $role2);

        Permission::create(['name' => 'question.index', 'description' => 'Show question list'])->syncRoles($role1, $role2,$role3);
        Permission::create(['name' => 'question.create', 'description' => 'Add question'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.store', 'description' => 'Store question'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.edit', 'description' => 'Edit question'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.update', 'description' => 'Update question'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.destroy', 'description' => 'Delete question'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'question.link.index', 'description' => 'Show question link list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.link.create', 'description' => 'Add question link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.link.store', 'description' => 'Store question link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'question.link.destroy', 'description' => 'Delete question link'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'solution.index', 'description' => 'Show solution list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.create', 'description' => 'Create solution'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.store', 'description' => 'Store solution'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.update', 'description' => 'Update solution'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.edit', 'description' => 'Edit solution'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.destroy', 'description' => 'Delete solution'])->syncRoles($role1, $role2, $role3, $role4);
        
        Permission::create(['name' => 'solution.link.index', 'description' => 'Show solution link list'])->syncRoles($role1, $role2,$role3);
        Permission::create(['name' => 'solution.link.show', 'description' => 'Editar solution link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.link.create', 'description' => 'Create solution link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.link.store', 'description' => 'Store solution link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.link.destroy', 'description' => 'Delete solution link'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'solution.question.index', 'description' => 'Show solution question link list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.question.create', 'description' => 'Add solution question link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.question.store', 'description' => 'Store solution question link'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'solution.question.destroy', 'description' => 'Delete solution question link'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'organization.index', 'description' => 'Show organization list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.create', 'description' => 'Create organization'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'organization.store', 'description' => 'Store organization'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.edit', 'description' => 'Edit organization'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.update', 'description' => 'Update organization'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.destroy', 'description' => 'Delete organization'])->syncRoles($role1, $role2);

        Permission::create(['name' => 'organization.group.index', 'description' => 'Show organization group list'])->syncRoles($role1, $role2,$role3);
        Permission::create(['name' => 'organization.group.show', 'description' => 'Show organization group'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.group.create', 'description' => 'Create organization group'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.group.store', 'description' => 'Store organization group'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'organization.group.destroy', 'description' => 'Delete organization group'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'admin.users.index', 'description' => 'Show users list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'admin.users.edit', 'description' => 'Assign user rol'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'admin.roles.index', 'description' => 'Show roles list'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'admin.roles.create', 'description' => 'Add rol'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'admin.roles.edit', 'description' => 'Edit rol'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'admin.roles.destroy', 'description' => 'Delete rol'])->syncRoles($role1, $role2, $role3, $role4);

        Permission::create(['name' => 'admin.permissions.index', 'description' => 'Show permissions list'])->syncRoles($role1, $role2, $role3, $role4);
        // Permission::create(['name' => 'admin.roles.create', 'description' => 'Add rol'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'admin.permissions.edit', 'description' => 'Assign permissions'])->syncRoles($role1, $role2, $role3, $role4);
        Permission::create(['name' => 'admin.permissions.destroy', 'description' => 'Delete rol'])->syncRoles($role1, $role2, $role3, $role4);
        



    }
}
