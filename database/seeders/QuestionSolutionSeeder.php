<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\QuestionSolution;

class QuestionSolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '13' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '14' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '15' ]);
        // QuestionSolution::create(['questionId' => '1', 'solutionId' => '16' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '13' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '14' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '15' ]);
        // QuestionSolution::create(['questionId' => '2', 'solutionId' => '16' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '1' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '2' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '3' ]);
        // QuestionSolution::create(['questionId' => '3', 'solutionId' => '4' ]);

        QuestionSolution::create(['questionId' => '1', 'solutionId' => '1' ]);
        QuestionSolution::create(['questionId' => '1', 'solutionId' => '2' ]);
        QuestionSolution::create(['questionId' => '1', 'solutionId' => '3' ]);
        QuestionSolution::create(['questionId' => '1', 'solutionId' => '4' ]);
        QuestionSolution::create(['questionId' => '1', 'solutionId' => '5' ]);
        QuestionSolution::create(['questionId' => '2', 'solutionId' => '1' ]);
        QuestionSolution::create(['questionId' => '3', 'solutionId' => '2' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '6' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '7' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '8' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '9' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '10' ]);
        QuestionSolution::create(['questionId' => '4', 'solutionId' => '11' ]);

        QuestionSolution::create(['questionId' => '5', 'solutionId' => '12' ]);
        QuestionSolution::create(['questionId' => '5', 'solutionId' => '13' ]);
        QuestionSolution::create(['questionId' => '5', 'solutionId' => '14' ]);
        QuestionSolution::create(['questionId' => '6', 'solutionId' => '15' ]);
        QuestionSolution::create(['questionId' => '7', 'solutionId' => '16' ]);
        QuestionSolution::create(['questionId' => '8', 'solutionId' => '17' ]);
        QuestionSolution::create(['questionId' => '9', 'solutionId' => '18' ]);


    }

}
