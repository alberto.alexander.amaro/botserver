<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Organization;


class WordFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name=$this->faker->unique()->sentence(3);
        return [
            'wordName'=> $name,
            'categoryId'=>Category::all()->random()->categoryId,
            'scope'=>$this->faker->randomElement([1, 2]),
            'orgId'=> Organization::all()->random()->orgId,        
        ];
    }



}
