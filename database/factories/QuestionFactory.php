<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Organization;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name=$this->faker->unique()->sentence(2);
        return [
            'questionName'=> $name,
            'scope'=>$this->faker->randomElement([1, 2]),
            'orgId'=> Organization::all()->random()->orgId,        
        ];
    }



}
