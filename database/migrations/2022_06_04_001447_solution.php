<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Solution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solution', function (Blueprint $table) {
            $table->id('solutionId');
            $table->string('introMessage', 150)->default('');
            $table->string('shortText', 120);
            $table->string('longText', 1600)->default('');
            $table->string('subject', 40);
            $table->string('language', 12)->default('en');
            $table->string('url', 180)->default('');
            $table->decimal('cost', 14,2)->default(0);
            $table->string('filename', 250)->default('');
            $table->string('filetype', 12)->default('');
            $table->enum('openinChatbot', ['y', 'n'])->default('n');            
            $table->string('scope', 1);
            $table->string('orgId', 7);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solution');
    }
}
