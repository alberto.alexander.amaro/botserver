<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->id('orgId', 7);
            $table->string('orgName', 250);
            $table->string('orgContact', 250);
            $table->string('orgEmail', 250);
            $table->string('orgLanguage', 12);

            $table->string('orgMultilanguage', 1)->default(1);
            $table->string('orgKaas', 1)->default(0);
            $table->string('orgRPA', 1)->default(0);
            
            $table->string('orgIsDefault', 1);
            $table->string('orgIsActive', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organitations');
    }
}
