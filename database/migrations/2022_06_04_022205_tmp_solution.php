<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TmpSolution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
              Schema::create('tmp_solution', function (Blueprint $table) {
                $table->id('tmpSolutionId');
                $table->string('solutionId', 11);
                $table->string('longName', 1000);
                $table->string('shortName', 120);
                $table->string('subject', 40);
                $table->string('language', 12);
                $table->string('scope', 3);
                $table->string('orgId', 7);
                $table->timestamps();
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('tmp_solution');
    }
}
