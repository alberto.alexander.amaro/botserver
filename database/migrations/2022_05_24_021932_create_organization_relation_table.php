<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_relation', function (Blueprint $table) {
            $table->id('orgRelationId', 6);
            $table->string('leftOrgId', 6);
            $table->string('orgLinkId', 11);
            $table->string('rightOrgId', 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organitation_relations');
    }
}
