
import React, { StrictMode } from 'react';
import ReactDOM  from 'react-dom';
import "./index.css";
import App from "./src/AppCont";
// import App from "./src/components/ChatSystem";
// import { createRoot } from "react-dom/client";
const domNode = document.getElementById("root");
// const root = createRoot(domNode);

// root.render(
//     <React.StrictMode>
//         <App />
//     </React.StrictMode>
// );
ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  domNode
);

// import { createRoot } from 'react-dom/client';

// const root = createRoot(document.getElementById('root'));
// root.render(
//   <StrictMode>
//     <App />
//   </StrictMode>
// );
