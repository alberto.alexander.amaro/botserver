export const uniqueIdJsx = () => {
  return Math.round(Date.now() * Math.random());
};
export const uniqueIdMessage = () => {
  function char4() {
    return Math.round(Date.now() * Math.random())
      .toString(16)
      .slice(-4);
  }
  return (
    char4() +
    char4() +
    "-" +
    char4() +
    "-" +
    char4() +
    "-" +
    char4() +
    "-" +
    char4() +
    char4() +
    char4()
  );
};
