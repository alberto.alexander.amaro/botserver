import React, { useEffect, useRef, useState, useContext } from "react";
import ReactDOM from "react-dom";
import { AppContext } from "../contexts/AppContext.jsx";
import ReactScrollToBottom from "react-scroll-to-bottom";
// import "./styles.css";
import { uniqueIdMessage, uniqueIdJsx } from "./instanceChat";
// import "./Chat.css";
import "./area.css";
import "./bot.css";
import css from "./chat.module.css";

export default function Chatbot() {
    const appContext = useContext(AppContext);
    const {
        token,
        userId,
        logout,
        changeAuthStatusBotChat,
    } = appContext;
    const [messages, setMessages] = useState([]);
    const [msuser, setMsuser] = useState([]);
    const [inquiry, setInquiry] = useState("");

    useEffect(() => {
        fetchData();
    }, []);
    const appUrl = "https://phplaravel-1158841-4038893.cloudwaysapps.com/api/v1/spertabot";
    const misDatos = {
        orgid: 1,
        userid: 1,
        language: "en",
        inquiry: "",
    };

    const fetchConfig = {
        method: "POST",
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        body:
            msuser.length === 0
                ? JSON.stringify(misDatos)
                : JSON.stringify(msuser),
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Retry-After": 3600,
            charset: "utf-8",
            "Access-Control-Allow-Origin": "URLs to trust of allow",
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT",
            "Access-Control-Allow-Headers": "Accept",
            "Cache-Control": null,
            "X-Requested-With": null,
        },
    };

    let messagesm = [];
    const fetchData = async () => {
        try {
            const data = await fetch(appUrl, fetchConfig);
            const answers = await data.json();
            console.log(answers); /*setAnswer(users.answer)*/ //setAnswers(answers);
            // console.log(css); /*setAnswer(users.answer)*/ //setAnswers(answers);
            answers.message !== "" &&
                (messagesm = {
                    id: uniqueIdMessage(),
                    name: answers.message,
                    subject: answers.rFormat,
                    rFormat: "text",
                    classe: "bot",
                    type: "bot",
                });
            setMessages((messages) => [...messages, messagesm]);
            answers.answer.forEach((el) => {
                messagesm = {
                    id: uniqueIdMessage(),
                    valuem: el.value,
                    name: el.text,
                    subject: el.subject,
                    rFormat: answers.rFormat,
                    type: "bot",
                };
                setMessages((messages) => [...messages, messagesm]);
                console.log(userId);
            });
        } catch (error) {
            console.log("error", error);
        }
    };
    const accionClic = (text) => {
        setMsuser({
            id: uniqueIdMessage(),
            orgid: 1,
            userid: 1,
            language: "en",
            inquiry: text,
        });
        msuser.length !== 0 && msuser.inquiry !== "" && updateMessUser();
    };

    const handleChange = (e) => {
        setMsuser({
            id: uniqueIdMessage(),
            orgid: 1,
            userid: 1,
            language: "en",
            inquiry: e.target.value,
        });
        setInquiry(e.target.value);
    };
    const updateMessUser = () => {
        messagesm = {
            id: uniqueIdMessage(),
            name: msuser.inquiry,
            rFormat: "text",
            classe: "user",
            type: "user",
        };
        setMessages((messages) => [...messages, messagesm]);
        fetchData();
        setInquiry("");
        setMsuser([]);
    };
    function handleSubmit(e) {
        e.preventDefault();

        msuser.length !== 0 && msuser.inquiry !== "" && updateMessUser();
    }
    console.log(messages);
    return (
        <>
            <div className={css.spertabot_chat_container}>
                <div className={css.spertabot_chat_header}>
                    <div>
                        <p>SpertaBot</p>
                    </div>
                    <div className="py-3">
                        <button
                            className="hover:bg-black text-white text-base hover:text-blue-500 text-center rounded py-1 ml-8 w-46"
                            onClick={() => changeAuthStatusBotChat()}
                        >
                            Transfer to a Live Agent.
                        </button>
                    </div>
                    <div className="py-3">
                        <button
                            className="hover:bg-white text-white hover:text-blue-500 text-center rounded ml-4"
                            onClick={() => logout()}
                        >
                            X
                        </button>
                    </div>
                </div>

                {/* <div className="messageArea" ref={messageEl}> */}
                <ReactScrollToBottom
                    initialScrollBehavior="smooth"
                    className="messageArea"
                >
                    <ol className={css.chat}>
                        {messages.map((item) =>
                            item.classe === "user" ? (
                                <li className="visitor" key={item.id}>
                                    <div className="msg">
                                        {<p>{item.name}</p>}
                                    </div>
                                </li>
                            ) : item.classe === "bot" ? (
                                <li className="chatbot" key={item.id}>
                                    <div className="msg">
                                        {<p>{item.name}</p>}
                                    </div>
                                </li>
                            ) : item.name === "Transfer to a Live Agent." ? (
                                <button
                                    type={item.rFormat}
                                    className={item.rFormat}
                                    key={item.id}
                                    value={item.name}
                                    onClick={() => item.valuem}
                                >
                                    {item.valuem}
                                </button>
                            ) : (
                                <input
                                    type={item.rFormat}
                                    className={item.rFormat}
                                    key={item.id}
                                    value={item.name}
                                    onClick={(event) =>
                                        accionClic(item.valuem, event)
                                    }
                                />
                            )
                        )}
                    </ol>
                </ReactScrollToBottom>
                {/* </div> */}

                <form
                    className={css.spertabot_chat_input_form}
                    onSubmit={handleSubmit}
                >
                    <input
                        id="m"
                        className={css.spertabot_chat_input}
                        placeholder="Write your message"
                        onChange={handleChange}
                        value={inquiry}
                    />
                    <button
                        className={css.spertabot_chat_btn_send}
                        name="Enviar"
                        type="submit"
                    >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            className="feather feather-send"
                        >
                            <line x1="22" y1="2" x2="11" y2="13"></line>
                            <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                        </svg>
                    </button>
                </form>
            </div>
        </>
    );
}
