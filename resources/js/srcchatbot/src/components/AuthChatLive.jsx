/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useRef, useContext } from "react";
import { AppContext } from "../contexts/AppContext.jsx";
import ReactScrollToBottom from "react-scroll-to-bottom";
import Echo from "laravel-echo";
import { uniqueIdMessage, uniqueIdJsx } from "./instanceChat";
import "./Chat.css";
import "./area.css";
import "./bot.css";

import axios from "axios";
const endpoint = "http://localhost:8001/api/";
const Message = () => {
    const appContext = useContext(AppContext);
    const {
        token,
        userId,
        logout,
        authStatusChat,
        changeAuthStatusChatBot,
        changeAuthStatusBotChat,
    } = appContext;

    const [messages, setMessages] = useState([]);
    const [msuser, setMsuser] = useState([]);
    const [inquiry, setInquiry] = useState("");
    const [receiverId, setReceiverId] = useState("");

    const config = {
        headers: {
            Authorization: "Bearer " + token,
        },
    };

    useEffect(() => {
        fetchData();
    }, []);

    const misDatos = {
        orgid: 1,
        userid: 1,
        language: "en",
        inquiry: "",
    };

    let messagesm = [];
    const fetchData = () => {
        const echo = new Echo({
            broadcaster: "pusher",
            key: "public-key-123",
            auth: {
                headers: {
                    Authorization: "Bearer " + token,
                },
            },
            wsHost: "127.0.0.1",
            wsPort: 6001,
            cluster: "mt1",
            forceTLS: false,
            disableStats: true,
        });

        echo.join("chat")
            .here((data) => {
                data.forEach((el) => {
                    if (el[1] == userId) {
                        el[0].forEach((eli) => {
                            if (eli.user_id !== userId) {
                                setReceiverId(eli.user_id);
                                messagesm = {
                                    id: eli.id,
                                    valuem: eli.message,
                                    name: eli.message,
                                    subject: eli.subject,
                                    rFormat: "text",
                                    classe: "bot",
                                    type: "bot",
                                };
                            } else {
                                messagesm = {
                                    id: eli.id,
                                    valuem: eli.message,
                                    name: eli.message,
                                    subject: eli.subject,
                                    rFormat: "text",
                                    classe: "user",
                                    type: "bot",
                                };
                            }

                            setMessages((messages) => [...messages, messagesm]);
                        });
                    }
                });
            })
            .listen(".server-event", (data) => {
                console.log(data);
                if (data.data.user_id !== userId) {
                    messagesm = {
                        id: data.data.id,
                        valuem: data.data.message,
                        name: data.data.message,
                        subject: data.data.subject,
                        rFormat: "text",
                        classe: "bot",
                        type: "bot",
                    };
                } else {
                    messagesm = {
                        id: data.data.id,
                        valuem: data.data.message,
                        name: data.data.message,
                        subject: data.data.subject,
                        rFormat: "text",
                        classe: "user",
                        type: "bot",
                    };
                }
                console.log(messagesm.classe);
                setMessages((messages) => [...messages, messagesm]);
            });
    };

    const accionClic = (text) => {
        setMsuser({
            id: uniqueIdMessage(),
            orgid: 1,
            userid: 1,
            language: "en",
            inquiry: text,
        });
        msuser.length !== 0 && msuser.inquiry !== "" && updateMessUser();
    };

    const handleChange = (e) => {
        setMsuser({
            id: uniqueIdMessage(),
            orgid: 1,
            userid: 1,
            language: "en",
            inquiry: e.target.value,
        });
        setInquiry(e.target.value);
    };
    const updateMessUser = () => {
        messagesm = {
            id: uniqueIdMessage(),
            name: msuser.inquiry,
            rFormat: "text",
            classe: "user",
            type: "user",
        };
        setMessages((messages) => [...messages, messagesm]);
        fetchData();
        setInquiry("");
        setMsuser([]);
    };

    const store = async (e) => {
        e.preventDefault();
        if (!inquiry) return;
        await axios.post(
            endpoint + "messages",
            {
                message: inquiry,
                user_id: userId,
                receiver: receiverId,
                is_seen: 0,
            },
            config
        );

        setInquiry("");
    };

    return (
        <div className="spertabot-chat-container">
            <div className="spertabot-chat-header">
                {/*<img src={chatbothd} className="App-chatbot-header" alt="" />Rivero Visual Group</div>*/}
                {/* <img src={chatbothd} className="App-chatbot-header" alt="" /> */}
                <p>SpertaChat</p>
                <span className="App-chatbot-header-span"></span>
                <div className="py-3">
                    <button
                        // className="hover:bg-white text-white hover:text-blue-500 text-center rounded py-1 mx-2 w-32 border border-white"
                        className="hover:bg-black text-white text-base hover:text-blue-500 text-center rounded py-1 ml-4 w-46"
                        onClick={() => changeAuthStatusChatBot()}
                    >
                        Transfer to Chatbot
                    </button>
                </div>
                <div>
                    <button
                        className="hover:bg-white text-white text-small hover:text-blue-500 text-center rounded  ml-4"
                        // className="hover:bg-white text-white text-small hover:text-blue-500 text-center rounded"
                        onClick={() => logout()}
                    >
                        X
                    </button>
                </div>
            </div>
            {/* <div className="spertabot-chat-inner-container"> */}
            {/*<div className="spertabot-chat-message-container">*/}
            <ReactScrollToBottom className="messageArea">
                <ol className="chat">
                    {messages.map((item) =>
                        item.classe === "user" ? (
                            <li className="visitor" key={uniqueIdMessage()}>
                                <div className="msg">
                                    {/*<div className="spertabot-chat-bot-message-arrow-user"></div>*/}
                                    {<p>{item.name}</p>}
                                </div>
                            </li>
                        ) : item.classe === "bot" ? (
                            <li className="chatbot" key={uniqueIdMessage()}>
                                <div className="msg">
                                    {/*<div className="spertabot-chat-bot-message-arrow"></div>*/}
                                    {<p>{item.name}</p>}
                                </div>
                            </li>
                        ) : (
                            <input
                                type={item.rFormat}
                                className={item.rFormat}
                                key={item.id}
                                value={item.name}
                                onClick={(event) =>
                                    accionClic(item.valuem, event)
                                }
                            />
                        )
                    )}
                </ol>
                <div style={{ paddingBottom: "15px" }} />
            </ReactScrollToBottom>
            {/* <div className="spertabot-chat-input-container"> */}
            <form className="spertabot-chat-input-form" onSubmit={store}>
                <input
                    id="m"
                    className="spertabot-chat-input"
                    placeholder="Write your message"
                    onChange={handleChange}
                    value={inquiry}
                    // onKeyDown={handleOnKeyDown}
                />
                <button
                    className="spertabot-chat-btn-send"
                    name="Enviar"
                    type="submit"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-send"
                    >
                        <line x1="22" y1="2" x2="11" y2="13"></line>
                        <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                    </svg>
                </button>
            </form>
            {/* </div> */}
            {/* </div> */}
        </div>
    );
};
export default Message;
