import React, { useContext } from "react";
import {
    NOT_LOGGED_IN,
    LOG_IN_FORM,
    SIGN_UP_FORM,
    LOGGED_IN,
} from "../constants/AuthStatus";
import AuthNotLoggedIn from "./AuthNotLoggedIn";
import AuthSignup from "./AuthSignup";
import AuthLogin from "./AuthLogin";
import Chat from "./Chat";
import { AppContext } from "../contexts/AppContext";

export default function AuthContainer () {
    const appContext = useContext(AppContext);
    const { authStatus, errorMessage } = appContext;
    const showNotLoggedIn = authStatus === NOT_LOGGED_IN ? "" : "hidden";
    const showLoginForm = authStatus === LOG_IN_FORM ? "" : "hidden";
    const showSignupForm = authStatus === SIGN_UP_FORM ? "" : "hidden";
    const showLoggedIn = authStatus === LOGGED_IN ? "" : "hidden";

    return (
        <div className="w-full">
            <div className={showNotLoggedIn + " justify-end"}>
                <AuthNotLoggedIn />
            </div>
            <div className={showLoginForm + " justify-end"}>
                <AuthLogin option="login" />
            </div>
            <div className={showSignupForm + " justify-end"}>
                <AuthSignup option="signup" />
            </div>

            {/* {showLoggedIn === LOGGED_IN ? ( */}
            <div className={showLoggedIn + " justify-end"}>
                <React.StrictMode>
                    <Chat />
                </React.StrictMode>
            </div>

        </div>
    );
};

