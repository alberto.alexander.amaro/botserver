
export default function ChatSystem() {


    return (

<>
                <div className="hamburger">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        className="feather feather-menu mail-menu d-lg-none"
                    >
                        <line x1="3" y1="12" x2="21" y2="12"></line>
                        <line x1="3" y1="6" x2="21" y2="6"></line>
                        <line x1="3" y1="18" x2="21" y2="18"></line>
                    </svg>
                </div>
                <div className="user-list-box">
                    <div className="search">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            className="feather feather-search"
                        >
                            <circle cx="11" cy="11" r="8"></circle>
                            <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                        </svg>
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search"
                        />
                    </div>
                    <div className="people">
                        <div className="person" data-chat="person6">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Nia Hillyer"
                                        >
                                            Alberto Amaro
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">How do you do?</span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person1">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Sean Freeman"
                                        >
                                            Sean Freeman
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        I was wondering...
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person2">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Alma Clarke"
                                        >
                                            Alma Clarke
                                        </span>
                                        <span className="user-meta-time">
                                            1:44 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        I've forgotten how it felt before
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person3">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Alan Green"
                                        >
                                            Alan Green
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        But we’re probably gonna need a new
                                        carpet.
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person4">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Shaun Park"
                                        >
                                            Shaun Park
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        It’s not that bad...
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person5">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Roxanne"
                                        >
                                            Roxanne
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person7">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Ernest Reeves"
                                        >
                                            Ernest Reeves
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person8">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Laurie Fox"
                                        >
                                            Laurie Fox
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person9">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Xavier"
                                        >
                                            Xavier
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person10">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Susan Phillips"
                                        >
                                            Susan Phillips
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person" data-chat="person11">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Dale Butler"
                                        >
                                            Dale Butler
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="person border-none" data-chat="person12">
                            <div className="user-info">
                                <div className="f-head">
                                    <img
                                        src="{{ asset('assets/img/90x90.jpg') }}"
                                        alt="avatar"
                                    />
                                </div>
                                <div className="f-body">
                                    <div className="meta-info">
                                        <span
                                            className="user-name"
                                            data-name="Grace Roberts"
                                        >
                                            Grace Roberts
                                        </span>
                                        <span className="user-meta-time">
                                            2:09 PM
                                        </span>
                                    </div>
                                    <span className="preview">
                                        Wasup for the third time like is you
                                        bling bitch
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="chat-box">
                    <div className="chat-not-selected">
                        <p>
                            {" "}
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                stroke="currentColor"
                                stroke-width="2"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                className="feather feather-message-square"
                            >
                                <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
                            </svg>{" "}
                            Click User To Chat
                        </p>
                    </div>

                    <div className="overlay-phone-call">
                        <div className="">
                            <div className="calling-user-info">
                                <div className="">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        stroke="currentColor"
                                        stroke-width="2"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        className="feather feather-arrow-left-circle go-back-chat"
                                    >
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <polyline points="12 8 8 12 12 16"></polyline>
                                        <line
                                            x1="16"
                                            y1="12"
                                            x2="8"
                                            y2="12"
                                        ></line>
                                    </svg>
                                    <span className="user-name"></span>
                                    <span className="call-status">Calling...</span>
                                </div>
                            </div>

                            <div className="calling-user-img">
                                <div className="">
                                    <img
                                        src="assets/img/90x90.jpg"
                                        alt="dynamic-image"
                                    />
                                </div>

                                <div className="timer">
                                    <label className="minutes">00</label> :{" "}
                                    <label className="seconds">00</label>
                                </div>
                            </div>

                            <div className="calling-options">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-video switch-to-video-call"
                                >
                                    <polygon points="23 7 16 12 23 17 23 7"></polygon>
                                    <rect
                                        x="1"
                                        y="5"
                                        width="15"
                                        height="14"
                                        rx="2"
                                        ry="2"
                                    ></rect>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-mic switch-to-microphone"
                                >
                                    <path d="M12 1a3 3 0 0 0-3 3v8a3 3 0 0 0 6 0V4a3 3 0 0 0-3-3z"></path>
                                    <path d="M19 10v2a7 7 0 0 1-14 0v-2"></path>
                                    <line
                                        x1="12"
                                        y1="19"
                                        x2="12"
                                        y2="23"
                                    ></line>
                                    <line x1="8" y1="23" x2="16" y2="23"></line>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-plus add-more-caller"
                                >
                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-phone-off cancel-call"
                                >
                                    <path d="M10.68 13.31a16 16 0 0 0 3.41 2.6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7 2 2 0 0 1 1.72 2v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.42 19.42 0 0 1-3.33-2.67m-2.67-3.34a19.79 19.79 0 0 1-3.07-8.63A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91"></path>
                                    <line x1="23" y1="1" x2="1" y2="23"></line>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="overlay-video-call">
                        <img
                            src="assets/img/175x115.jpg"
                            className="video-caller"
                            alt="video-chat"
                        />
                        <div className="">
                            <div className="calling-user-info">
                                <div className="d-flex">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        stroke="currentColor"
                                        stroke-width="2"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        className="feather feather-arrow-left-circle go-back-chat"
                                    >
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <polyline points="12 8 8 12 12 16"></polyline>
                                        <line
                                            x1="16"
                                            y1="12"
                                            x2="8"
                                            y2="12"
                                        ></line>
                                    </svg>
                                    <div className="">
                                        <span className="user-name"></span>
                                        <div className="timer">
                                            <label className="minutes">00</label>:{" "}
                                            <label className="seconds">00</label>
                                        </div>
                                    </div>
                                    <span className="call-status">Calling...</span>
                                </div>
                            </div>

                            <div className="calling-user-img">
                                <div className="">
                                    <img
                                        src="assets/img/90x90.jpg"
                                        alt="dynamic-image"
                                    />
                                </div>
                            </div>
                            <div className="calling-options">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-phone switch-to-phone-call"
                                >
                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-mic switch-to-microphone"
                                >
                                    <path d="M12 1a3 3 0 0 0-3 3v8a3 3 0 0 0 6 0V4a3 3 0 0 0-3-3z"></path>
                                    <path d="M19 10v2a7 7 0 0 1-14 0v-2"></path>
                                    <line
                                        x1="12"
                                        y1="19"
                                        x2="12"
                                        y2="23"
                                    ></line>
                                    <line x1="8" y1="23" x2="16" y2="23"></line>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-plus add-more-caller"
                                >
                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    stroke="currentColor"
                                    stroke-width="2"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    className="feather feather-video-off cancel-call"
                                >
                                    <path d="M16 16v1a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h2m5.66 0H14a2 2 0 0 1 2 2v3.34l1 1L23 7v10"></path>
                                    <line x1="1" y1="1" x2="23" y2="23"></line>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="chat-box-inner">
                        <div className="chat-meta-user">
                            {/* <header className="msger-header">
                                <div className="msger-header-title">
                                    <i className="fas fa-comment-alt"></i>
                                    <span className="chatWith"></span>
                                    <span className="typing" style="display: none;">
                                        {" "}
                                        está escribiendo
                                    </span>
                                </div>
                                <div className="msger-header-options">
                                    <span className="chatStatus offline">
                                        <i className="fas fa-globe"></i>
                                    </span>
                                </div>
                            </header> */}
                        </div>
                        <div className="chat-conversation-box">
                            <div
                                id="chat-conversation-box-scroll"
                                className="chat-conversation-box-scroll"
                            >
                                <div className="chat" data-chat="person6"></div>
                            </div>
                        </div>
                        <div className="chat-footer">
                            <div className="chat-input">
                                <form className="msger-inputarea">
                                    <input
                                        type="text"
                                        className="msger-input"
                                        oninput="sendTypingEvent()"
                                        placeholder="Enter your message..."
                                    />
                                    <button
                                        type="submit"
                                        className="msger-send-btn"
                                    >
                                        Send
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </>

    );
}
