/* eslint-disable react-hooks/exhaustive-deps */
import Chatbot from "./Chatbot";
import Chatlive from "./AuthChatLive";
import React, { useState, useRef, useContext } from "react";
import { AppContext } from "../contexts/AppContext.jsx";
import { LOGGED_IN_CHAT } from "../constants/AuthStatus";

export default function Chat() {
    const appContext = useContext(AppContext);
    const { authStatusChat } = appContext;

    if (authStatusChat == LOGGED_IN_CHAT) {
        return (
            <div className="chat">
                <React.StrictMode>
                    <Chatlive />
                </React.StrictMode>
            </div>
        );
    } else {
        return (
            <div className="chat">
                <React.StrictMode>
                    <Chatbot />
                </React.StrictMode>
            </div>
        );
    }
}
