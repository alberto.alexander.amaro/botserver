export const NOT_LOGGED_IN = "not logged in";
export const LOG_IN_FORM = "log in form";
export const SIGN_UP_FORM = "sign up form";
export const LOGGED_IN = "logged_in";
export const LOGGED_IN_BOT = "logged in SpertaBot"
export const LOGGED_IN_CHAT = "logged in SpertaChat"
