/* eslint-disable prettier/prettier */
import React, { useState } from "react";
import axios from "axios";
import ReactDOM from "react-dom";
import {
  NOT_LOGGED_IN,
  LOG_IN_FORM,
  SIGN_UP_FORM,
  LOGGED_IN,
  LOGGED_IN_BOT,
  LOGGED_IN_CHAT,
} from "../constants/AuthStatus";

const AppContext = React.createContext();

const AppProvider = (props) => {
  let hostName = "";
  if (process.env.NODE_ENV === "development") {
    hostName = "https://phplaravel-1158841-4038893.cloudwaysapps.com/";
  } else if (process.env.NODE_ENV === "production") {
    // hostName = "https://authapi.bob-humphrey.com/";
    hostName = "https://phplaravel-1158841-4038893.cloudwaysapps.com/";
  }

  const [authStatus, setAuthStatus] = useState(NOT_LOGGED_IN);
  const [authStatusChat, setAuthStatusChat] = useState(LOGGED_IN_BOT);
  const [errorMessage, setErrorMessage] = useState("");
  const [userId, setUserId] = useState(0);
  const [userName, setUserName] = useState("");
  const [userNameInput, setUserNameInput] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [token, setToken] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const config = {
    headers: {
        Authorization: "Bearer " + token,
    },
};

  function changeAuthStatusLogin() {
    setAuthStatus(LOG_IN_FORM);
  }
  function changeAuthStatusChatBot() {
    setAuthStatusChat(LOGGED_IN_BOT);
  }
  function changeAuthStatusBotChat() {
    setAuthStatusChat(LOGGED_IN_CHAT);
  }


  function changeAuthStatusSignup() {
    setAuthStatus(SIGN_UP_FORM);
  }

  function handleUserNameInput(changeEvent) {
    let updatedUserName = changeEvent.target.value;
    setUserNameInput(updatedUserName);
  }

  function handleUserEmail(changeEvent) {
    let updatedUserEmail = changeEvent.target.value;
    setUserEmail(updatedUserEmail);
  }

  function handleUserPassword(changeEvent) {
    let updatedUserPassword = changeEvent.target.value;
    setUserPassword(updatedUserPassword);
  }

  const signup = () => {
    axios.defaults.withCredentials = true;
    // CSRF COOKIE
    axios.get(hostName + "sanctum/csrf-cookie").then(
      (response) => {
        console.log(response);
        // SIGNUP / REGISTER
        axios
          .post(hostName + "api/register", {
            name: userNameInput,
            email: userEmail,
            password: userPassword,
          })
          .then(
            (response) => {
              console.log(response.data.user);
              // GET USER
              axios.post(hostName + "api/login",{
                email: response.data.user.email,
                password: response.data.user.email,
              }).then(
                (response) => {
                  //console.log(response);
                  setUserId(response.data.id);
                  setUserName(response.data.name);
                  setToken(response.data.token);
                  setErrorMessage("");
                  setAuthStatus(LOGGED_IN);
                },
                // GET USER ERROR
                (error) => {
                  setErrorMessage("1 Could not complete the sign up");
                }
              );
            },
            // SIGNUP ERROR
            (error) => {
              if (error.response.data.errors.name) {
                setErrorMessage(error.response.data.errors.name[0]);
              } else if (error.response.data.errors.email) {
                setErrorMessage(error.response.data.errors.email[0]);
              } else if (error.response.data.errors.password) {
                setErrorMessage(error.response.data.errors.password[0]);
              } else if (error.response.data.message) {
                setErrorMessage(error.response.data.message);
              } else {
                setErrorMessage("2 Could not complete the sign up");
              }
            }
          );
      },
      // COOKIE ERROR
      (error) => {
        setErrorMessage("3 Could not complete the sign up");
      }
    );
  };

  const login = () => {
    axios.defaults.withCredentials = true;
    // CSRF COOKIE
    axios.get(hostName + "sanctum/csrf-cookie").then(
      (response) => {
        //console.log(response);
        // LOGIN
        axios
          .post(hostName + "api/login", {
            email: userEmail,
            password: userPassword,
          })
          .then(
            (response) => {
              console.log(response.data);
              setUserId(response.data.id);
              setUserName(response.data.name);
              setToken(response.data.token);
              setErrorMessage("");
              setUserPassword("");
              setAuthStatus(LOGGED_IN);

              // GET USER
              // axios.get(hostName + "api/user").then(
              //   (response) => {
              //     console.log(response);
              //     setUserId(response.data.id);
              //     setUserName(response.data.name);
              //     setErrorMessage("");
              //     setAuthStatus(LOGGED_IN);
              //   },
              //   // GET USER ERROR
              //   (error) => {
              //     setErrorMessage("4 Could not complete the login");
              //   }
              // );
            },
            // LOGIN ERROR
            (error) => {
              if (error.response) {
                setErrorMessage(error.response.data.message);
              } else {
                setErrorMessage("5 Could not complete the login");
              }
            }
          );
      },
      // COOKIE ERROR
      (error) => {
        setErrorMessage("6 Could not complete the login");
      }
    );
  };

  function logout() {

    axios.defaults.withCredentials = true;
    axios.post(
        hostName + "api/logout",
        {
            message: "inquiry",
            user_id: "userId",
            receiver: 'hola',
            is_seen: 0,
        },
        config
    );
    // axios.post(hostName + "api/logout", config);
    setUserId(0);
    setUserName("");
    setUserNameInput("");
    setUserEmail("");
    setUserPassword("");
    setAuthStatus(NOT_LOGGED_IN);
    changeAuthStatusChatBot()
  }

  return (
    <AppContext.Provider
      value={{
        authStatus,
        changeAuthStatusLogin,
        changeAuthStatusBotChat,
        changeAuthStatusChatBot,
        changeAuthStatusSignup,
        userId,
        // userName,
        // userNameInput,
        userEmail,
        // userPassword,
        token,
        handleUserNameInput,
        handleUserEmail,
        handleUserPassword,
        signup,
        login,
        authStatusChat,
        logout,
        errorMessage,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };

