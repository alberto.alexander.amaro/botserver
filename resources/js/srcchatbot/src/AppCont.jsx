/* eslint-disable prettier/prettier */
import React from "react";
// import { createRoot } from "react-dom/client";
import ReactDOM from "react-dom";
import "./css/tailwind.css";
import { AppProvider } from "./contexts/AppContext";
import AuthContainer from "./components/AuthContainer";
export default function AppContainer() {
    return (
      // <div className="flex w-full justify-center bg-blue-200 pt-16 pb-32">
        <div>
          <AppProvider>
          <React.StrictMode>
            <AuthContainer />
            </React.StrictMode>
          </AppProvider>

        </div>
      // </div>
    );
  }

// ReactDOM.render(
//     <AppProvider>
//         <React.StrictMode>
//             <AuthContainer />
//         </React.StrictMode>
//     </AppProvider>,
//     document.getElementById("root")
// );
