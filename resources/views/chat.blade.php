@extends('layouts.theme.app')
@section('content')
    <div class="layout-px-spacing">
        @can('solution.index')
            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">

                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ asset('/') }}">{{ trans('multi-lang.home') }}</a></li>
                                <li class="breadcrumb-item">{{ trans('multi-lang.mchat') }}</li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    <span>{{ trans('multi-lang.chat') }}</span>
                                </li>
                            </ol>
                        </nav>

                    </div>
                </li>
            </ul>
            <div class="widget-content widget-content-area br-6">

                <div class="row">

                    <div class="col-xl-12 col-lg-12 col-md-12">


                        <div id="root"></div>
                        @viteReactRefresh
                        @vite('resources/js/app.js')

                    </div>
                </div>

                <script src='https://use.fontawesome.com/releases/v5.0.13/js/all.js'></script>

                <a href="{{ asset('/') }}" class="btn btn-warning my-4 mx-3">{{ trans('multi-lang.back') }}</a>
            </div>
        @else
            <x-un-authorized />
        @endcan
    </div>

@stop
