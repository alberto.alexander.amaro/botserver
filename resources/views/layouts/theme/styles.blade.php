<link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.svg') }}" />
<link href="{{ asset('assets/css/loader.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/js/loader.js') }}"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/dt-global_style.css') }}">
<link href="{{ asset('plugins/notification/snackbar/snackbar.min.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->



<link href="{{ asset('plugins/flatpickr/flatpickr.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/noUiSlider/nouislider.min.css') }}" rel="stylesheet" type="text/css">
<!-- END THEME GLOBAL STYLES -->

<!--  BEGIN CUSTOM STYLE FILE  -->

<link href="{{ asset('plugins/flatpickr/custom-flatpickr.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/noUiSlider/custom-nouiSlider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/bootstrap-range-Slider/bootstrap-slider.css') }}" rel="stylesheet" type="text/css">
<!--  END CUSTOM STYLE FILE  -->


<!-- BEGIN GLOBAL MANDATORY STYLES -->

<!-- END GLOBAL MANDATORY STYLES -->
<link href="{{ asset('assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/dt-global_style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/forms/theme-checkbox-radio.css') }}">
<link href="{{ asset('assets/css/apps/invoice-list.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/elements/alert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bot.css') }}" />
<link href="{{ asset('assets/css/apps/mailing-chat.css') }}" rel="stylesheet" type="text/css" />
{{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('css/chat.css') }}">

{{-- <!-- <link rel="stylesheet" type="text/css" media="screen and (max-device-width: 700px)" href="{{ asset('assets/css/bot.css') }}" /> --> --}}
<!-- <link rel="stylesheet" type="text/css" media="screen and (min-width: 700px) and (max-device-width: 1366px)" href="https://bot.clicknowsolutions.com/css/bot.css" /> -->


<!-- <link href="https://bot.clicknowsolutions.com/css/bot.css" rel="stylesheet" type="text/css" /> -->
{{-- <link href="http://localhost:3000/css/bot.css" rel="stylesheet" type="text/css" /> --}}
<style>
    .table>thead>tr>th {
        background: rgb(14 23 38);
        color: #f8f9fa;
    }

    #sidebar ul.menu-categories.ps {
        padding-right: 0px;
    }

    /* .sidebar-wrapper {
        width: 238px;
    } */

    .sidebar-wrapper {
        width: 290px;
    }
    .footer-wrapper {
    padding: 10px 20px 10px 68px !important;
    display: inline-block;
    background: transparent;
    font-weight: 600;
    font-size: 12px;
    width: 100%;
    border-top-left-radius: 6px;
    display: flex;
    justify-content: space-between;
}
    .layout-px-spacing {
        padding: 0 20px 0 68px !important;
        min-height: calc(100vh - 170px) !important;
    }
    .main-container {
        padding: 0 0 0 45px;
    }

    .breadcrumb-one .breadcrumb-item.active {
        color: #3b3f5c;
        font-weight: 600;
    }


    div.dataTables_wrapper div.dataTables_length label {
        color: #212324;
        font-weight: 600;
    }

    /* .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item.action-edit {
        background: #e7f7ff;
        color: #2196f3;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu button.dropdown-item.action-delete {
        background: #fff5f5;
        color: #e7515a;
    } */
    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu button.dropdown-item.action-delete {
        background: #fdcfcf;
        color: #e7515a;
        padding: 6px 11px;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item svg {
        width: 16px;
        height: 16px;
        margin-right: 7px;
        vertical-align: text-top;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu button.dropdown-item svg {
        width: 16px;
        height: 16px;
        margin-right: 7px;
        vertical-align: text-top;
    }

    .dt--top-section1 {
        margin: 110px 21px 110px 21px;

    }

    /* DISPONIBLE PARA SER AGREGADO EN EL CASO DE APROBACION POR EL PROFESOR GABRIEL CARRILLO */
    /* input[disabled], select[disabled], textarea[disabled], input[readonly], select[readonly], textarea[readonly] {
    cursor: not-allowed;
    background-color: #1abc9c  !important;
    color: #bfc9d4;
} */
    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item.action-link {
        background: #7fc33533;
        color: #00b126;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item.action-link svg {
        color: #00b126;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item.action-link2 {
        background: #ffe00778;
        color: #1d092a;
    }

    .table>tbody>tr>td .dropdown:not(.custom-dropdown-icon):not(.custom-dropdown) .dropdown-menu a.dropdown-item.action-link2 svg {
        color: #9600b1;
    }

    .alert {
        position: relative !important;
        padding: .75rem 1.25rem !important;
        padding-top: 0.75rem !important;
        padding-right: 1.25rem !important;
        padding-bottom: 0.75rem !important;
        padding-left: 1.25rem !important;
        margin-bottom: 0px !important;
    }

    .header-container .navbar .language-dropdown a.dropdown-toggle img {
        width: 30px;
        height: 32px;
        margin: 0px 0px;
    }

    .table>tbody>tr>td {
        font-size: 14px;

    }
</style>
<!-- @if(session('language') == 'es')
<style>
    .sidebar-wrapper {
        width: 264px;
    }

    .layout-px-spacing {
        padding: 0 20px 0 40px !important;
        min-height: calc(100vh - 170px) !important;
    }
</style>
@endif
@if(session('language') == 'fr')
<style>
    .sidebar-wrapper {
        width: 290px;
    }

    .layout-px-spacing {
        padding: 0 20px 0 68px !important;
        min-height: calc(100vh - 170px) !important;
    }
</style>
@endif -->

<style>
    .blockui-growl-message {
        display: none;
        text-align: left;
        padding: 15px;
        background-color: #455a64;
        color: #fff;
        border-radius: 3px;
    }

    .blockui-animation-container {
        display: none;
    }

    .multiMessageBlockUi {
        display: none;
        background-color: #455a64;
        color: #fff;
        border-radius: 3px;
        padding: 15px 15px 10px 15px;
    }

    .multiMessageBlockUi i {
        display: block
    }
</style>

@livewireStyles
