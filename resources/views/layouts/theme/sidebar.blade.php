<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>

        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu">
                <a href="{{ asset('/') }}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>{{trans('multi-lang.home') }}</span>
                    </div>
                </a>
            </li>
            @if (
            auth()->user()->can('word.index') ||
            auth()->user()->can('category.index') ||
            auth()->user()->can('question.index') ||
            auth()->user()->can('solution.index')
            )
            <li class="menu">
                <a href="#Knowledge" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map">
                            <polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon>
                            <line x1="8" y1="2" x2="8" y2="18"></line>
                            <line x1="16" y1="6" x2="16" y2="22"></line>
                        </svg>
                        <span>{{trans('multi-lang.kbase') }}</span>
                    </div>

                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Knowledge" data-parent="#accordionExample">
                    @can('word.index')
                    <li>
                        <a href="{{ asset('word') }}"> {{trans('multi-lang.word') }} </a>
                    </li>
                    @endcan
                    @can('category.index')
                    <li>
                        <a href="{{ asset('category') }}"> {{trans('multi-lang.category') }} </a>
                    </li>
                    <li>
                        <a href="{{ asset('category/livewire') }}"> {{trans('multi-lang.category') }} Livewire </a>
                    </li>
                    @endcan
                    @can('question.index')
                    <li>
                        <a href="{{ asset('question') }}"> {{trans('multi-lang.question') }} </a>
                    </li>
                    @endcan
                    @can('solution.index')
                    <li>
                        <a href="{{ asset('solution') }}"> {{trans('multi-lang.solution') }} </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @can('organization.manager')
            <li class="menu">
                <a href="#app" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu">
                            <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect>
                            <rect x="9" y="9" width="6" height="6"></rect>
                            <line x1="9" y1="1" x2="9" y2="4"></line>
                            <line x1="15" y1="1" x2="15" y2="4"></line>
                            <line x1="9" y1="20" x2="9" y2="23"></line>
                            <line x1="15" y1="20" x2="15" y2="23"></line>
                            <line x1="20" y1="9" x2="23" y2="9"></line>
                            <line x1="20" y1="14" x2="23" y2="14"></line>
                            <line x1="1" y1="9" x2="4" y2="9"></line>
                            <line x1="1" y1="14" x2="4" y2="14"></line>
                        </svg>
                        <span>{{trans('multi-lang.morganization') }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="app" data-parent="#accordionExample">
                    @can('organization.index')
                    <li>
                        <a href="{{ asset('organization') }}"> {{trans('multi-lang.organization') }} </a>
                    </li>
                    @endcan
                    <li>
                        <a href="{{ asset('organization/group') }}"> {{trans('multi-lang.mgorganization') }} </a>
                    </li>
                    @can('organization.indexgg')
                    <li>
                        <a href="apps_todoList.html"> Frontend messages </a>
                    </li>
                    <li>
                        <a href="apps_notes.html"> Backend messages </a>
                    </li>
                    <li>
                        <a href="apps_scrumboard.html">Portals</a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('access.manager')
            <li class="menu">
                <a href="#components" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                        </svg>
                        <span>{{trans('multi-lang.maccess') }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="components" data-parent="#accordionExample">
                    @can('admin.users.index')
                    <li>
                        <a href="{{ asset('admin/users') }}"> {{trans('multi-lang.user') }} </a>
                    </li>
                    @endcan
                    @can('admin.roles.index')
                    <li>
                        <a href="{{ asset('admin/roles') }}"> {{trans('multi-lang.role') }} </a>
                    </li>
                    @endcan
                    @can('admin.permissions.index')
                    <li>
                        <a href="{{ asset('admin/permissions') }}"> {{trans('multi-lang.permission') }} </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('access.manager')
            <li class="menu">
                <a href="#chat" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                        </svg>
                        <span>{{trans('multi-lang.mchat') }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="chat" data-parent="#accordionExample">
                    @can('admin.users.index')
                    <li>
                        <a href="{{ asset('chat') }}"> {{trans('multi-lang.chat') }} </a>
                    </li>
                    @endcan

                </ul>
            </li>
            @endcan
        </ul>
    </nav>
</div>
