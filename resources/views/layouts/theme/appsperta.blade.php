<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Spertatek | A Virtual Assitant for Modern Organizations </title>
    @include('layouts.theme.styles')
</head>

<body>
    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-border text-warning align-self-center "></div>
            </div>
        </div>
    </div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    @include('layouts.theme.header')

    <!--  END NAVBAR  -->

    <!--  BEGIN NAVBAR  -->

    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg></a>

        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        @include('layouts.theme.sidebar')
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            @yield('content')
            @include('layouts.theme.footer')
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->



    <main>
        <input type="checkbox" id="mostrar-modal" name="modal" />
        <label for="mostrar-modal">
            <img src="https://bot.spertatek.net/img/chatbot.svg" class="App-chatbot" alt="" />
        </label>
        <div id="modal">

            <iframe allowfullscreen src="http://localhost:8000/bot" class="chatbot" frameborder="0" scrolling="no"></iframe>
        </div>
    </main>


    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    @include('layouts.theme.scriptssperta')
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>
