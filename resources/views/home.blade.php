@extends('layouts.theme.app')
@section('content')
<div class="layout-px-spacing">
    <ul class="navbar-nav flex-row">
        <li>
            <div class="page-header">

                <nav class="breadcrumb-one" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">{{trans('multi-lang.home') }}</li>
                    </ol>
                </nav>

            </div>
        </li>
    </ul>
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

            <div class="widget-content widget-content-area br-6">

                <div class="col-sm-12">

                    <div class="dt--top-section1 text-center py-5">
                        <h2>{{trans('multi-lang.subTitle-application') }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop