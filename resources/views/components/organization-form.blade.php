<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Name', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>

    <div class="col-sm-10">
        {!! Form::text('orgName', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '250', 'placeholder' => 'Enter organization name']) !!}
        @error('orgName')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">

    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Contact', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-10">
        {!! Form::text('orgContact', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '250', 'placeholder' => 'Enter organization contact name']) !!}
        @error('orgContact')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Email', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-10">
        {!! Form::text('orgEmail', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '250', 'placeholder' => 'Enter organization contact email']) !!}
        @error('orgEmail')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Language', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-4">
        {!! Form::select('orgLanguage', $orgLanguage, null, ['class' => 'form-control']) !!}
        @error('orgLanguage')
        <br>
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Multilanguage', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-2">
        <div>
            {!! Form::radio('orgMultilanguage', 1, false ) !!}
            {!! Form::label('Active', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgMultilanguage', 0, true) !!}
            {!! Form::label('Active', 'No') !!}
        </div>

        @error('orgMultilanguage') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>
</div>

<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Kaas', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-2">
        <div>
            {!! Form::radio('orgKaas', 1, false ) !!}
            {!! Form::label('Active', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgKaas', 0, true) !!}
            {!! Form::label('Active', 'No') !!}
        </div>

        @error('orgKaas') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>
</div>
<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'RPA', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-2">
        <div>
            {!! Form::radio('orgRPA', 1, false ) !!}
            {!! Form::label('Active', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgRPA', 0, true) !!}
            {!! Form::label('Active', 'No') !!}
        </div>

        @error('orgRPA') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>
</div>

<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Default', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-2">

        @if($isDefault == false)
        <div>
            {!! Form::radio('orgIsDefault', 1, false, ['disabled']) !!}
            {!! Form::label('Default', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgIsDefault', 0, true) !!}
            {!! Form::label('Default', 'No') !!}
        </div>
        @else
        <div>
            {!! Form::radio('orgIsDefault', 1, false) !!}
            {!! Form::label('Default', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgIsDefault', 0, true) !!}
            {!! Form::label('Default', 'No') !!}
        </div>
        @endif
        @error('orgIsDefault') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>

</div>

<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', 'Active', ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-2">
        <div>
            {!! Form::radio('orgIsActive', 1, true ) !!}
            {!! Form::label('Active', 'Yes') !!}
        </div>
        <div>
            {!! Form::radio('orgIsActive', 0) !!}
            {!! Form::label('Active', 'No') !!}
        </div>

        @error('orgIsActive') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>
</div>