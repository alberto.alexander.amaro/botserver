<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', $solution, ['class' => 'text-dark']) !!}
        {!! Form::hidden('id', $id, null) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-5">
        {!! Form::select('hostId', $host, null, ['class' => 'form-control']) !!}
        @error('hostId')
        <br>
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', $linked , ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-5">
        {!! Form::select('memberId', $member, null, ['class' => 'form-control']) !!}
        @error('memberId')
        <br>
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>