<div class="row layout-top-spacing">
    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <div class="col-sm-12">
                <div class="dt--top-section">
                    <div class="">
                        {{ $slot }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>