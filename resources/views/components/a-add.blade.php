<div class="alinger form-group row">
    <div class="col-sm-10">
        @can($psv)
        <a href="{{ $href }}" class="btn btn-success mb-2">{{ $action }}</a>
        @endcan
    </div>
    @if (session('status'))
    <x-alert />
    @endif
</div>