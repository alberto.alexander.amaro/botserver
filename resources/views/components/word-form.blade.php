<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', trans('multi-lang.word'), ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-6">
        {!! Form::text('wordName', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '250', 'placeholder' => trans('multi-lang.enter').' '.trans('multi-lang.word')]) !!}
        @error('wordName')
        <span class="form-text text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', trans('multi-lang.category'), ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-4">
        {!! Form::select('categoryId', $categories, null, ['class' => 'form-control']) !!}
        @error('categoryId')
        <br>
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', trans('multi-lang.organization'), ['class' => 'text-dark']) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-4">
        {!! Form::select('orgId', $organizations, null, ['class' => 'form-control']) !!}
        @error('orgId')
        <br>
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="form-group row mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        <label class="text-dark">{{ trans('multi-lang.scope') }}</label>
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-6">
        @foreach ($scope as $s)
        <div>
            {!! Form::radio('scope', $s['id'], $s['selected']) !!}
            {!! Form::label('Scope', trans('multi-lang.'.$s['name']) ) !!}
        </div>
        @endforeach
        @error('scope') <span class="text-danger er"> {{ $message }}</span>@enderror
    </div>
</div>