<div class="form-group row  mb-4">
    <div class="col-sm-2 col-form-label col-form-label-sm">
        {!! Form::label('name', trans('multi-lang.category')) !!}
        <span class="col-form-label col-form-label-sm  text-danger">*</span>
    </div>
    <div class="col-sm-6">
        {!! Form::text('categoryName', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '250', 'placeholder' => trans('multi-lang.enter').' '.trans('multi-lang.category'), 'wire:model'=>'categoryName']) !!}
        @error('categoryName')
        <span class="text-danger">{{$message}}</span>
        @enderror
    </div>
</div>