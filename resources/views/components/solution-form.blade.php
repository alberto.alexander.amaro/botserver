 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.si-msg'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         {!! Form::text('introMsg', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '150', 'placeholder' => 'Enter intro message']) !!}
         @error('introMsg')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.ss-text'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         {!! Form::text('shortText', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '120', 'placeholder' => 'Enter short text']) !!}
         @error('shortText')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>

 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name',  trans('multi-lang.sl-text'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-10">
         {!! Form::textarea('longText', null, ['class' => 'form-control textarea', 'id'=>'textarea', 'rows'=>'7', 'maxlength' => '64000', 'placeholder' => 'Enter long text']) !!}
         @error('longText')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-subject'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         {!! Form::text('subject', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '120', 'placeholder' => 'Enter subject' , 'maxlength' => '40']) !!}
         @error('subject')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-language'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-2">
         {!! Form::select('language', $language, null, ['class' => 'form-control']) !!}
         @error('language')
         <br>
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-url'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         {!! Form::text('url', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '180', 'placeholder' => 'Enter url', 'maxlength' => '180']) !!}
         @error('url')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>

 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-cost'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-3">
         {!! Form::text('cost', null, ['class' => 'form-control placement-bottom-right', 'maxlength' => '17', 'placeholder' => 'Enter cost', 'maxlength' => '17']) !!}
         @error('cost')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-file'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         {!! Form::file('filename', ['class' => 'form-control-file', 'accept' =>'image/x-png, image/gif, image/jpg', 'maxlength' => '250' ]) !!}
         @error('filename')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>

 <div class="form-group row mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-type'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         @foreach ($filetype as $s)
         <div>
             {!! Form::radio('filetype', $s['id'], $s['selected']) !!}
             {!! Form::label('Filetype', $s['name']) !!}
         </div>
         @endforeach
         @error('filetype') <span class="text-danger er"> {{ $message }}</span>@enderror
     </div>
 </div>
 <div class="form-group row mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-o-chatbot'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         @foreach ($openin as $s)
         <div>
             {!! Form::radio('openin', $s['id'], $s['selected']) !!}
             {!! Form::label('openinChatbot', $s['name']) !!}
         </div>
         @endforeach
         @error('openin') <span class="text-danger er"> {{ $message }}</span>@enderror
     </div>
 </div>
 <div class="form-group row  mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.organization'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-4">
         {!! Form::select('orgId', $organizations, null, ['class' => 'form-control']) !!}
         @error('orgId')
         <br>
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row mb-4">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.scope'), ['class' => 'text-dark']) !!}
         <span class="col-form-label col-form-label-sm  text-danger">*</span>
     </div>
     <div class="col-sm-6">
         @foreach ($scope as $s)
         <div>
             {!! Form::radio('scope', $s['id'], $s['selected']) !!}
             {!! Form::label('Scope', trans('multi-lang.'.$s['name']) ) !!}
         </div>
         @endforeach
         @error('scope') <span class="text-danger er"> {{ $message }}</span>@enderror
     </div>
 </div>
 <div class="form-group row  mb-3">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-date1'), ['class' => 'text-dark']) !!}
     </div>
     <div class="col-sm-2">
         {!! Form::text('date1', null, ['class' => 'form-control flatpickr flatpickr-input active', 'id' => 'basicFlatpickr', 'placeholder' => 'Select Date..']) !!}
         @error('date1')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
 <div class="form-group row  mb-3">
     <div class="col-sm-2 col-form-label col-form-label-sm">
         {!! Form::label('name', trans('multi-lang.s-date2'), ['class' => 'text-dark']) !!}
     </div>
     <div class="col-sm-2">
         {!! Form::text('date2', null, ['class' => 'form-control flatpickr flatpickr-input active', 'id' => 'basicFlatpickr1', 'placeholder' => 'Select Date..']) !!}
         @error('date2')
         <span class="text-danger">{{$message}}</span>
         @enderror
     </div>
 </div>
