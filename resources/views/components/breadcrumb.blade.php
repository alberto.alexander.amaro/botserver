<div>
    <ul class="navbar-nav flex-row">
        <li>
            <div class="page-header">

                <nav class="breadcrumb-one" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Access Manager</a></li>
                        <!-- <li class="breadcrumb-item" aria-current="page"><a href="{{ asset('admin/users') }}">Users</a></li> -->
                        <li class="breadcrumb-item active bt-dark" aria-current="page"><span>User</span></li>
                    </ol>
                </nav>

            </div>
            <h2><span class="text-dark">User</span></h2>
        </li>
    </ul>
</div>