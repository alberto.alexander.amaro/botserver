<th wire:click="order('{{$th}}')" class="table-th text-white text-center">
    {{ $thName}}
    @if($sort == $th)
    @if ($direction == 'asc')
    <i class="fas fa-sort-alpha-up-alt ml-2 mt-1"></i>
    @else
    <i class="fas fa-sort-alpha-down-alt ml-2 mt-1"></i>
    @endif
    @else
    <i class="fas fa-sort ml-2 mt-1"></i>
    @endif
</th>